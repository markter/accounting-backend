'use strict';

const _       = require('lodash');
const path    = require('path');

const config = {
    APP_NAME: 'iChoose App',

    FRONTEND_URL: 'ichoose.loc',

    PORT: 8000,

    SALT: 'iCh0os3NaCl',

    SECRET: 'iCh0os3s3cr3t',

    ENCRYPT: 'aes192',

    TOKEN_ALGO: 'HS256',

    TOKEN_EXPIRATION: 60*60*24*30 * 7, // expires in 7 days,

    CRYPTO_ALGO: 'AES-256-CTR',

    CRYPTO_BUFFER: Buffer.alloc(32),

    IV_LENGTH: 16,

    USER: '',

    PASS: '',

    UPLOAD_DIR: path.normalize(__dirname + '/../uploads/'),
    ASSETS_DIR: path.normalize(__dirname + '/../assets'),
    VIEWS_DIR: path.normalize(__dirname + '/../views'),
    LOGS_DIR: path.normalize(__dirname + '/../logs'),

    use: (env) => {
        _.assign(config, require(__dirname + '/env/' + env));
        return config;
    },

};

if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
}

module.exports = config.use(process.env.NODE_ENV);