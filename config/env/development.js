'use strict';

module.exports = {
    ENV: 'development',

    CORS: {
        allowed_headers: 'Content-Type, Accept, x-access-token',
        allowed_origins_list: [
            'cdi.loc',
            'http://localhost'
        ],
        allowed_methods: 'GET, POST, PUT, DELETE',
        allow_credentials: true
    },

    /* MASTER_DB: {
        host: 'localhost',
        user: 'dev',
        password: 'dev',
        database: 'accountingExpress'
    }, */
    
    MASTER_DB: {
        host: 'accounting-db.cbbp0eb7qggx.ap-southeast-1.rds.amazonaws.com',
        user: 'accounting',
        password: 'I~ch123oosemoako$',
        database: 'EADev'
    },

    QUOTE_DB: {
        host: 'accounting-db.cbbp0eb7qggx.ap-southeast-1.rds.amazonaws.com',
        user: 'accounting',
        password: 'I~ch123oosemoako$',
        database: 'EQDev'
    },

    REDISDB: {
        host: 'localhost',
        port: 6379
    }
    
};