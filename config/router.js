'use strict';

const importer     = require('anytv-node-importer');
const verify_token = require(__dirname+'/../controllers/auth').verify_token;

module.exports = (router) => {
    const __   = importer.dirloadSync(__dirname + '/../controllers');

    router.del = router.delete;

    //! USERS MODULE
    router.get  ('/users',                   verify_token, __.user.retrieve);
    router.post ('/user',                                  __.user.create);
    router.get  ('/user/:id',                verify_token, __.user.retrieve_id);
    router.put  ('/user/:id',                verify_token, __.user.update);
    router.del  ('/user/:id',                verify_token, __.user.delete);
    router.post ('/user/change_password',    verify_token, __.user.change_password);

    //! AUTH MODULE
    router.post ('/auth/uname_email',                      __.auth.username);
    router.post ('/auth/login',                            __.auth.login);
    router.post ('/auth/logout',             verify_token, __.auth.logout);

    //! GLOBAL TAGS
    router.get ('/category',                               __.global.category);

    //! ISSUANCE MODULE 
    router.get ('/issuance',                               __.issuance.retrieve);
    router.get ('/issuance/:id',                           __.issuance.retrieve_id);

    //! SALES MODULE
    router.post('/sales',                                  __.sales.create);
    router.get ('/sales/verify/:policy_no',                __.sales.verify_unique);

    router.get ('/sales',                                  __.sales.retrieve);
    router.get ('/sales/manage/:sales_id',                 __.sales.manage_cash_receipts);
    router.get ('/sales/soa/:policy_no',                   __.sales.soa);
    
    //! COMMISSION MODULE
    router.post('/commission',                             __.commission.compute);

    //! COLLECTION MODULE
    router.get ('/initial_payments',                       __.collections.retrieve_initial);
    router.post('/collection',                             __.collections.create);
    router.get ('/collection/:sales_id',                   __.collections.retrieve_id);
    router.put ('/collection/:id',                         __.collections.update);

    //! COLLECTION REPORTS MODULE
    router.get ('/collections',                            __.collections.retrieve);
    router.get ('/collections/property/:sales_id',         __.collections.property);
    router.put ('/collections/property/:policy_no',        __.collections.update_property);

    // router.get ('/collections/export',                     __.collections.export_data);

    //! OVERDUE MONTHLY MODULE
    router.get ('/overdue_monthly',                        __.overdue.retrieve);

    //! REMITTANCE RECORD MODULE
    router.get ('/remittance/record',                      __.remittance.retrieve_record);
    router.post('/remmittance/record',                     __.remittance.create_record);
    router.get ('/remittance/record/pool',                 __.remittance.retrieve_pool);

    router.get ('/remittance/policy',                      __.remittance.retrieve_policy);

    //! ACCOUNTING MODULE
        //! CASH RECIPTS BOOK
    router.get ('/crb',                                    __.crb.retrieve);

        //! DEPOSITS 
    router.get ('/deposits',                               __.deposits.retrieve);
    router.get ('/deposits/manage/:crb_id',                __.deposits.manage);
    router.post('/deposits/:crb_id',                       __.deposits.create_deposit); //! Unknown computation need to clarify to frontend

    router.get ('/deposits/summary',                       __.deposits.retrieve_summary);

    //! BANK FILING MODULE
    router.get ('/bank_filing',                            __.filing.retrieve);
    router.get ('/bank_filing/:sales_id',                  __.filing.retrieve_id);

    //! EXPORTS MODDULES
    router.get ('/export/issuance',                        __.issuance.export_data);
    router.get ('/export/sales',                           __.sales.export_data);
    router.get ('/export/initial_payments',               __.collections.export_data);
    router.get ('/export/remittance',                      __.remittance.export_data);


    router.all('*', (req, res) => {
        res.status(404)
           .send({message: 'Nothing to do here.'});
    });

    return router;
};