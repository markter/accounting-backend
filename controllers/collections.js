'use strict';

const mysql         = require('anytv-node-mysql');
const winston       = require('winston');

const util          = require(__dirname + '/../helpers/util');
const data_export   = require(__dirname + '/../helpers/data_export');

exports.retrieve_initial = (req, res, next) => {

    let page                = parseInt(req.query.page || 0),
        perPage             = parseInt(req.query.per_page || 10),
        start_from          = (page - 1) < 0 ? 0 : (page -1)  * perPage,
        query               = '',
        count;

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',  
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        bank                = req.query.bank,
        payment             = req.query.payment,
        search              = req.query.search,
        filter_date         = req.query.filter_date,
        payee               = req.query.payee,
        policy_role         = req.query.policy_role,
        mode_of_delivery    = req.query.mode_of_delivery,
        is_cancel            = req.query.is_cancel;

    let bank_query,
        payment_query;
       
    function start () {

        if (search){

            query += ` AND (c.policy_no LIKE '${ search }'`;
            query += ` OR c.date_received LIKE '${ search }'`;
            query += ` OR c.bd_date_deposited LIKE '${ search }`;
            query += ` OR s.assured_name LIKE '${ search }')`;
 
        }

        if (mode_of_delivery) query += `  AND mode_of_delivery = '${ mode_of_delivery }'`;

        if (is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;

        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;

        if (payee) query += ` AND c.payee = '${ payee }'`; 

        if (policy_role) query += ` AND s.policy_role = '${ policy_role }'`;

        (payment) ? payment_query = `AND c.mode_of_payment = '${ payment }'` 
                  : payment_query = `AND (c.mode_of_payment = 'bank_deposit' OR c.mode_of_payment = 'paypal')`;

        (bank)  ? bank_query = `AND c.bd_bank_account = '${ bank }'` 
                : bank_query = `AND (c.bd_bank_account = 'BPI' OR c.bd_bank_account = 'BDO' OR c.bd_bank_account = 'GCash'
                                OR c.bd_bank_account = 'Paypal' OR c.bd_bank_account = 'SECURITY BANK')`;

        mysql.use('master')
        .query(
            `
                SELECT  COUNT(*) AS count
                FROM    crb_data c
                LEFT JOIN sales s
                ON      c.sales_id = s.sales_id
                WHERE   c.period_term_of_payment = '1'
                AND     (s.mode_of_delivery <> 'Deliver & Collect' OR s.mode_of_delivery IS NULL)
                ${ query }
                ${ bank_query }
                ${ payment_query }
            `,
            data
        )
        .end();

    }

    function data (err, result) {

        if (err) {
            winston.error('Error in total counts of initial payments');
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Initial payments not found');

        count = result[0].count;

        mysql.use('master')
        .query(
            `
                SELECT      s.sales_id, s.assured_name, c.amount_received, c.policy_no,
                            c.date_received, bd_date_deposited, c.mode_of_payment,
                            c.bd_bank_account, s.is_collected,
                            s.delivery_address, s.mode_of_delivery, s.agent_sales_person
                FROM        crb_data c
                LEFT JOIN   sales s
                ON          c.sales_id = s.sales_id
                WHERE       c.period_term_of_payment = '1'
                AND         (s.mode_of_delivery <> 'Deliver & Collect' OR s.mode_of_delivery IS NULL)
                ${ query }
                ${ bank_query }
                ${ payment_query }
                ORDER BY    c.sales_id DESC
                LIMIT       ${ start_from }, ${ perPage }
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving initial payments');
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Initial payments not found');

        let pagination = {};

        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / perPage);

        res.data({ result, pagination: pagination })
        .send();

    }

    start ();

}

exports.retrieve = (req, res, next) => {

    let page        = parseInt(req.query.page || 1),
        perPage     = parseInt(req.query.per_page || 10),
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * perPage,
        count;
    
    let search              = req.query.search,
        filter_date         = req.query.filter_date,
        filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        is_cancel           = req.query.is_cancel,
        filter_sort_name    = req.query.sort_name;

    let query = ``;

    function start () {

        if(search) {
            query += ` AND (policy_no LIKE '%${ search }%'`;
            query += ` OR agent_sales_person '%${ search }%'`;
            query += ` OR reference_quote_no '%${ search }%'`;
            query += ` OR assured_name '%${ search }%'`;
            query += ` OR contact_no '%${ search }%'`;
            query += ` OR insurance_name '%${ search }%'`;
            query += ` OR classification '%${ search }%'`;
            query += ` OR od_premium '%${ search }%'`;
            query += ` OR aon_premium '%${ search }%'`;
            query += ` OR bi_pd_premium '%${ search }%'`;
            query += ` OR gross_premium '%${ search }%'`;
            query += ` OR temporary_balance '%${ search }%'`;
            query += ` OR temporary_sum_total_amount '%${ search }%'`;
            query += ` OR actual_sum_total_amount '%${ search }%'`;
            query += ` OR actual_balance '%${ search }%'`;
            query += ` OR last_payment_date '%${ search }%'`;
            query += ` OR actual_fully_paid '%${ search }%'`;
            query += ` OR policy_role '%${ search }%')`;
        }

        if(is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;

        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;

        if (filter_sort_name) { 
            query += ' ORDER BY '+ filter_sort_name.replace("'","") + " " + sorting_direction.replace("'", "");
        } else {
            query += ' ORDER BY sales_id desc, date_created DESC';
        }

        mysql.use('master')
        .query(
            `
                SELECT  COUNT(*) AS count
                FROM    sales
                WHERE   1
                ${ query }
            `,
            data
        )
        .end();

    }

    function data (err, result) {

        if (err) {
            winston.error('Error in total counts of collections');
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Collection records not found');

        count = result[0].count;

        mysql.use('master')
        .query(
            `
                SELECT  sales_id, policy_no, assured_name,
                        car_make_model, email_address, 
                        effectivity_date, last_payment_date
                FROM    sales
                WHERE   1
                ${ query }
                LIMIT   ${ start_from }, ${ perPage }
            `,
        send_response
        )
        .end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving collection records');
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Collection records not found');

        let pagination = {};

        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / perPage);

        res.data({ result, pagination: pagination })
        .send();

    }

    start();

}

exports.property = (req, res, next) => {

    function start () {

        mysql.use('master')
        .query(
            `
                SELECT  s.sales_id, s.policy_no, s.next_follow_up_date,
                        s.assured_name, c.receipt_number, c.notes,
                        c.collections_officer, c.pullout_date,
                        s.payment_memo, s.collection_notes
                FROM    sales s
                LEFT JOIN collections c
                ON      s.sales_id = c.sales_id
                WHERE   s.sales_id = '${ req.params.sales_id }'
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving collection property');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Collection property not found');
        
        res.data(result[0])
        .send();

    }

    start ();

}

exports.update_property = (req, res, next) => {

    const data = util._get
    .form_data({
        _collection_notes: '',
        _next_follow_up_date: '',
        _payment_memo: ''
    })
    .from(req.body);
    
    function start () {

        mysql.use('master')
        .query(
            `
                UPDATE  sales
                SET     ?
                WHERE   policy_no = '${ req.params.policy_no }'
            `,
            data,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in updating collection property');
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_UPDATED', 'No collection property was updated');
        
        data.policy_no = req.params.policy_no;

        res.data({
            message: 'Successfully update a collection property',
            result: data
        }).send();

    }

    start ();

}

exports.retrieve_id = (req, res, next) => {

    function start () {

        mysql.use('master')
        .query(
            `
                SELECT  id, sales_id, receipt_number,
                        pullout_date, collections_officer,
                        notes, date_created
                FROM    collections
                WHERE   sales_id = '${ req.params.sales_id }'
            `,
        send_response
        )
        .end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving collection record');
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Collection record not found');

        res.data(result[0])
        .send();

    }

    start();

}

exports.create = (req, res, next) => {
    
    const data = util._get
    .form_data({
        sales_id: 1,
        receipt_number: '',
        collections_officer: '',
        pullout_date: '',
        _notes: ''
    })
    .from(req.body);

    let collections_id;

    function start () {

        if (data instanceof Error) return res.error('INC_DATA', data.message);

        mysql.use('master')
        .query(
            `
                INSERT INTO collections
                SET ?
            `,
            data,
            insert_response
        )
        .end();

    }

    function insert_response (err, result) {

        if (err) {
            winston.error('Error in creating collection record');
            return next(err);
        }

        if (result.affectedRows === 0) return res.error('NO_RECORD_CREATED', 'Collection record was not created');

        collections_id = result.insertId;

        mysql.use('master')
        .query(
            `
                UPDATE  sales
                SET     is_collected = '1'
                WHERE   sales_id = '${ data.sales_id }'
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in creating collection record');
            return next(err);
        }

        if (result.affectedRows === 0) return res.error('NO_RECORD_UPDATED', 'Data for collection record was not created');

        res.data({
            message:                'Successfully created a collection record',
            id:                     collections_id,
            sales_id:               data.sales_id,
            receipt_number:         data.receipt_number,
            pullout_date:           data.pullout_date,
            collections_officer:    data.collections_officer,
            notes:                  data.notes
        })
        .send();

    }

    start();

}

exports.update = (req, res, next) => {
    
    const data = util._get
    .form_data({
        _pullout_date: '',
        _receipt_number: '',
        _collections_officer: '',
        _notes: ''
    })
    .from(req.body);

    function start () {

        mysql.use('master')
        .query(
            `
                UPDATE  collections
                SET     ?
                WHERE   id = '${ req.params.id }'
            `,
            data,
            send_response
        )
        .end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in updating collection record');
            return next(err);
        }

        if (result.affectedRows === 0) return res.error('NO_RECORD_UPDATED', 'Collection record was not updated');

        res.data({
            message:                'Successfully updated a collection record',
            collections_id:         req.params.collections_id,
            receipt_number:         data.receipt_number,
            pullout_date:           data.pullout_date,
            collections_officer:    data.collections_officer,
            notes:                  data.notes
        })
        .send();

    }

    start();

}

exports.export_data = (req, res, next) => {

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',  
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        bank                = req.query.bank,
        payment             = req.query.payment,
        filter_crb          = req.query.filter_crb,
        filter_date         = req.query.filter_date,
        payee               = req.query.payee,
        policy_role         = req.query.policy_role,
        query               = '';

    let bank_query,
        payment_query;

    let column_name         = '',
        agency_name         = '',
        workbook,
        sheet,
        column_order,
        start_row_at;

    function start () {

        (policy_role == 'ichoose') 
            ? agency_name = 'Ichoose.ph'
            : agency_name = 'Bestlink'; 

        if (filter_crb){

            query += ' and (c.policy_no LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' or c.date_received LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' or c.bd_date_deposited LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' or s.assured_name LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ')';
    
        }

        if (filter_date) query += ' AND DATE(' + filter_date +') BETWEEN '+ mysql.escape(filter_date_from) +' AND ' + mysql.escape(filter_date_to);
        if (payee) query += ' and c.payee=' + mysql.escape(payee); 
        if (policy_role) query += ' AND s.policy_role='+mysql.escape(policy_role);

        (payment) ? payment_query = `AND c.mode_of_payment = '${ payment }'` 
                    : payment_query = `AND (c.mode_of_payment = 'bank_deposit' OR c.mode_of_payment = 'paypal')`;

        (bank)  ? bank_query = `AND c.bd_bank_account = '${ bank }'` 
                : bank_query = `AND (c.bd_bank_account = 'BPI' OR c.bd_bank_account = 'BDO' OR c.bd_bank_account = 'GCash'
                                OR c.bd_bank_account = 'Paypal' OR c.bd_bank_account = 'SECURITY BANK')`;

        mysql.use('master')
        .query(
            `
                SELECT      c.date_received, c.mode_of_payment, c.amount_received,
                            s.assured_name, c.bd_bank_account, c.bd_date_deposited,
                            c.policy_no, s.reference_quote_no, s.delivery_address,
                            cs.receipt_number, cs.pullout_date, cs.collections_officer,
                            cs.notes                            
                FROM        sales s
                LEFT JOIN   crb_data c
                ON          s.sales_id = c.sales_id
                LEFT JOIN   collections cs
                ON          s.sales_id = cs.sales_id
                WHERE       c.period_term_of_payment = '1'
                ${ query }
                ${ bank_query }
                ${ payment_query }
                ORDER BY    c.crb_id DESC
            `,
            exports
        )
        .end();

    }

    function exports (err, result) {

        // console.log(result);

        if (err) console.log(err);

        workbook = Workbook.fromFileSync(__dirname + "/../exceltemplate/template_initial_payments.xlsx");
        column_order = [ 
            'date_received', 'mode_of_payment', 'amount_received', 
            'assured_name', 'bd_bank_account', 'bd_date_deposited',
            'policy_no', 'reference_quote_no', 'delivery_address',
            'receipt_number', 'pullout_date', 'collections_officer',
            'notes'
        ];
        start_row_at = 7;
        sheet = workbook.getSheet(0);
        sheet.getCell("A2").setValue(agency_name);


        for (var rowNum = 0; rowNum < result.length; rowNum++) {
            var row = sheet.getRow(rowNum+start_row_at);

            for (var colNum = 1; colNum <= column_order.length; colNum++) {

                column_name = column_order[(colNum -1)];
                var cell = row.getCell(colNum);

                cell.setValue(result[rowNum][column_name]);
            }
        }

        var end_row_num = start_row_at + rowNum;
        var sub_total_row_num = end_row_num + 5;


        var cell_subtotal = sub_total_row_num.getCell
            
        var outfile = __dirname + "/../exceltemplate/initial_payments_report.xlsx";
        workbook.toFileSync(outfile); 
        res.download(outfile, 'initial_payments.xlsx');  

    }

    start ();

}

exports.export_data = (req, res, next) => {

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',  
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        bank                = req.query.bank,
        payment             = req.query.payment,
        search              = req.query.search,
        filter_date         = req.query.filter_date,
        payee               = req.query.payee,
        policy_role         = req.query.policy_role,
        mode_of_delivery    = req.query.mode_of_delivery,
        is_cancel           = req.query.is_cancel;

    let bank_query          = '',
        payment_query       = '',
        query               = '',
        initial_payments_data;
       
    function start () {

        if (search){

            query += ` AND (c.policy_no LIKE '${ search }'`;
            query += ` OR c.date_received LIKE '${ search }'`;
            query += ` OR c.bd_date_deposited LIKE '${ search }`;
            query += ` OR s.assured_name LIKE '${ search }')`;
 
        }

        if (mode_of_delivery) query += `  AND mode_of_delivery = '${ mode_of_delivery }'`;

        if (is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;

        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;

        if (payee) query += ` AND c.payee = '${ payee }'`; 

        if (policy_role) query += ` AND s.policy_role = '${ policy_role }'`;

        (payment) ? payment_query = `AND c.mode_of_payment = '${ payment }'` 
                  : payment_query = `AND (c.mode_of_payment = 'bank_deposit' OR c.mode_of_payment = 'paypal')`;

        (bank)  ? bank_query = `AND c.bd_bank_account = '${ bank }'` 
                : bank_query = `AND (c.bd_bank_account = 'BPI' OR c.bd_bank_account = 'BDO' OR c.bd_bank_account = 'GCash'
                                OR c.bd_bank_account = 'Paypal' OR c.bd_bank_account = 'SECURITY BANK')`;

        mysql.use('master')
        .query(
            `
                SELECT      c.date_received, c.mode_of_payment, c.amount_received,
                            s.assured_name, c.bd_bank_account, c.bd_date_deposited,
                            c.policy_no, s.reference_quote_no, s.delivery_address,
                            cs.receipt_number, cs.pullout_date, cs.collections_officer,
                            cs.notes                            
                FROM        sales s
                LEFT JOIN   crb_data c
                ON          s.sales_id = c.sales_id
                LEFT JOIN   collections cs
                ON          s.sales_id = cs.sales_id
                WHERE       c.period_term_of_payment = '1'
                ${ query }
                ${ bank_query }
                ${ payment_query }
                ORDER BY    c.crb_id DESC
            `,
            to_export
        ).end();

    }

    function to_export (err, result) {

        //! If Error, diplay error message
        if (err) {
            winston.error('Error in retrieving sales');
            return next(err);
        }
        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');
        //! Store retrieved data
        initial_payments_data = result;
        //! Put the stored data in req.body
        req.body.issuance_data = initial_payments_data;
        //! Redirect to sales export library
        data_export.initial_payments_export(req, res, next);

    }

    start();

}