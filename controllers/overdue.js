'use strict'

const mysql         = require('anytv-node-mysql');
const winston       = require('winston');
const _             = require('lodash');

const util          = require(__dirname + '/../helpers/util');
const data_export   = require(__dirname + '/../helpers/data_export');


exports.retrieve = (req, res, next) => {

    let page                = parseInt(req.query.page) || 1,
        per_page            = parseInt(req.query.per_page) || 10,
        start_from          = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination          = {},
        count;

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        filter_date         = req.query.filter_date,
        search              = req.query.search,
        filter_sort_name    = req.query.sort_name,
        sorting_direction   = req.query.sorting_direction,
        policy_role         = req.query.policy_role,
        overdue_date        = req.query.overdue_date,
        next_follow_up_date = req.query.next_follow_up_date,
        insurance_quoted    = req.query.insurance_quoted,
        params              = [];

    let overdue_query_array = [],
        overdue_query_join = '';

    let query = ``;

    function start () {

        if(search) {

            query += ' and (policy_no LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR agent_sales_person LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR reference_quote_no LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR assured_name LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR contact_no LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR insurance_name LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR classification LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR od_premium LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR aon_premium LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR bi_pd_premium LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR gross_premium LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR temporary_balance LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR temporary_sum_total_amount LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR actual_sum_total_amount LIKE ' + mysql.escape('%'+search+'%'); 
            query += ' OR actual_balance LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR last_payment_date LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR actual_fully_paid LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR policy_role LIKE ' + mysql.escape('%'+search+'%');
            query += ' )';

        }

        if (filter_date) query += ` AND DATE (${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        
        if (overdue_date) overdue_query_array.push(' last_payment_date < ' + mysql.escape(overdue_date) + ' - INTERVAL 30 DAY ');
        
        if (next_follow_up_date) overdue_query_array.push(' next_follow_up_date =' + mysql.escape(next_follow_up_date) + ' ' );
        
        overdue_query_join = overdue_query_array.join(' OR ');

        if (overdue_query_join) query += ` AND (${ overdue_query_join })`;
        
        if (insurance_quoted) query += ` AND insurance_quoted = '${ insurance_quoted }'`;
                    
        if (filter_sort_name) { 
            query += ' ORDER BY '+ filter_sort_name.replace("'","") + " " + sorting_direction.replace("'", "");
        } else {
            query += ` ORDER BY sales_id DESC, date_created DESC`;
        }

        mysql.use('master')
        .query(
            `
                SELECT COUNT(*) AS counts
                FROM    sales
                WHERE   1
                ${ query }
            `,
            retrieve_count
        ).end();

    }

    function retrieve_count (err, result, args, last_query){

        if (err) {
            winston.error('Error in selecting count overdue sales', last_query);
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Overdue accounts not found');
    
        count = result[0].counts;

        mysql.use('master')
        .query(
            `
                SELECT  sales_id, policy_no, assured_name, email_address,
                        contact_no, contact_no2, agent_sales_person,
                        insurance_name, classification, gross_premium,
                        total_premium, terms_of_payment, issuance_date,
                        effectivity_date, date_created, total_comm_compute,
                        total_comm_manual, last_payment_date, next_follow_up_date,
                        actual_sum_total_amount, actual_balance
                FROM    sales
                WHERE   1
                ${ query }
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();
        
    }

    function send_response (err, result, args, last_query) {

        if (err) {
            winston.error('Error in selecting overdue sales', last_query);
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Overdue accounts not found');
        
        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        res.data({
            result:result,
            pagination:pagination
        })
        .send();

    }

    start ();

}