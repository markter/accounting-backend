'use strict'

const mysql     = require('anytv-node-mysql');
const winston   = require('winston');

const util      = require(__dirname + '/../helpers/util');

exports.compute = (req, res, next) => {

    const data = util._get
        .form_data({
            insurance_name: '',
            classification: '',
            category: '',
            _od_premium: 1,
            _aon_premium: 1,
            _bi_pd_premium: 1,
            _gross_premium: 1,
            _total_premium: 1,
            _fmv: 1
        })
        .from(req.body);

    let aon_comm,
        bi_pd_comm,
        tpl_premium,
        od_theft_comm,
        total_commission;

    function start () {
        if (data instanceof Error) return res.error('INC_DATA', data.message);

        if (data.insurance_name == "Mercantile Insurance" && data.classification == "PC" && data.category == "Motor/Car") {
            od_theft_comm       = (data.od_premium - (data.fmv * 0.009)) * 1.247;
            aon_comm            = data.aon_premium * 0.20;
            bi_pd_comm          = data.bi_pd_premium * 0.25;
            total_commission    = od_theft_comm + aon_comm + bi_pd_comm;
            
            send_response();
        }

        if (data.insurance_name == 'Mercantile Insurance' && data.classification == 'CV' && data.category == "Motor/Car") {
            od_theft_comm       = (data.od_premium - (data.fmv * 0.007)) * 1.247;
            aon_comm            = data.aon_premium * 0.20;
            bi_pd_comm          = data.bi_pd_premium * 0.25;
            total_commission    = od_theft_comm + aon_comm + bi_pd_comm;

            send_response();
        }

        if (data.insurance_name == 'Standard Insurance' && data.classification == 'PC' && data.category == "Motor/Car") {
            od_theft_comm       = (data.od_premium - (data.fmv * 0.013)) * 1.247;
            aon_comm            = (data.aon_premium - (data.fmv * 0.0025)) * 1.247;
            bi_pd_comm          = data.bi_pd_premium * 0.25;
            total_commission    = od_theft_comm + aon_comm + bi_pd_comm;

            send_response();
        }

        if (data.insurance_name == 'Standard Insurance' && data.classification == 'CV' && data.category == "Motor/Car") {
            od_theft_comm       = (data.od_premium - (data.fmv * 0.008)) * 1.247;
            aon_comm            = (data.aon_premium - (data.fmv * 0.0025)) * 1.247;
            bi_pd_comm          = data.bi_pd_premium * 0.25;
            total_commission    = od_theft_comm + aon_comm + bi_pd_comm;

            send_response();
        }

        if (data.insurance_name == 'FPG Insurance' && data.category == "Motor/Car") {
            total_commission = data.gross_premium * 0.25;
            send_response();
        }

        if (data.insurance_name == 'Mercantile Insurance' && (data.classification == 'PC' || data.classification == 'CV') && (data.category == 'ECTPL' || data.category == 'ECTPL Website')) {
            total_commission = data.total_premium - 270;
                send_response();
        }

        if (data.insurance_name == 'Mercantile Insurance' && (data.category == 'ECTPL' || data.category == 'ECTPL Website') && data.classification == 'Motorcycle') {
            total_commission = data.total_premium - 180;
            send_response();
        }

        if (data.insurance_name == 'Mercantile Insurance' && (data.category == 'ECTPL' || data.category == 'ECTPL Website') && data.classification == 'Truck') {
            total_commission = tdata.otal_premium - 650;
            send_response();
        }

        if (data.insurance_name == 'Standard Insurance' && (data.category == 'ECTPL' || data.category == 'ECTPL Website')) {
            total_commission = data.gross_premium * 0.35;
            send_response();
        }
        
    }

    function send_response () {
        res.data({
            od_theft_comm:      od_theft_comm,
            aon_comm:           aon_comm,
            bi_pd_comm:         bi_pd_comm,
            tpl_premium:        tpl_premium,
            total_commission:   total_commission
        })
        .send();
    }

    start();

}