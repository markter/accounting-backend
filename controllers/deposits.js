'use strict'

const mysql         = require('anytv-node-mysql');
const winston       = require('winston');
const _             = require('lodash');

const util          = require(__dirname + '/../helpers/util');
const data_export   = require(__dirname + '/../helpers/data_export');


exports.retrieve = (req, res, next) => {

    let per_page    = parseInt(req.query.per_page) || 10,
        page        = parseInt(req.query.page) || 1,
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination  = {},
        count;


    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        mode_of_payment     = req.query.mode_of_payment,
        bd_bank_account     = req.query.bd_bank_account,
        filter_date         = req.query.filter_date,
        search              = req.query.search,
        payee               = req.query.payee;

    let query = ``,
        collections = {},
        collection_amount;

    function start () {

        if (search){

            query += ` AND (s.assured_name LIKE '%${ search }'`;
            query += ` OR c.bd_reference_number LIKE '${ search }'`;
            query += ` OR c.cheque_number LIKE '${ search }'`;
            query += ` OR c.cheque_branch LIKE '${ search }')`;
 
        }

        if (filter_date) query += ` AND DATE(c.${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        
        if (mode_of_payment) query += ` AND c.mode_of_payment = '${ mode_of_payment }'`;

        if (bd_bank_account) query += ` AND c.bd_bank_account = '${ bd_bank_account }'`;
       
        if (payee) query += ` AND c.payee = '${ payee }'`; 
       
        mysql.use('master')
        .query(
            `
                SELECT  COUNT(c.crb_id) as counts
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'pending'
                ${ query }
            `,
            get_collections
        ).end();

    }

    function get_collections (err, result) {

        if (err) {
            winston.error('Error in selecting total counts for crb');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book records not found');

        count = result[0].counts;

        mysql.use('master')
        .query(
            `
                SELECT  c.mode_of_payment, sum(c.amount_received) as sub_total_amount
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'pending'
                ${ query }
                GROUP BY c.mode_of_payment
            `,
            get_data
        ).end();

    }

    function get_data (err, result) {

        if (err) {
            winston.error('Error in selecting total counts for crb');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book records not found');

        collection_amount = result;

        mysql.use('master')
        .query(
            `
                SELECT  c.crb_id, c.date_use_for_filter, c.mode_of_payment,
                        c.amount_received, s.assured_name,
                        c.policy_no, c.period_term_of_payment,
                        c.is_deposited, c.cheque_issuing_bank, 
                        c.cheque_branch, c.cheque_number, c.cheque_date,
                        c.bd_date_deposited, c.bd_bank_account, c.bd_reference_number
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'pending'
                ${ query }
                ORDER BY s.sales_id DESC, s.date_created DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in selecting sales');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales not found');

        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        collections.cash = 0;
        collections.cheque = 0;
        collections.bank_deposit = 0;
        collections.total_amount = 0;
        collections.paypal = 0;

        _.each(collection_amount, (amount) => {

            if (amount.mode_of_payment == 'cash') collections.cash += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'cheque') collections.cheque += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'bank_deposit') collections.bank_deposit += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'paypal') collections.paypal += amount.sub_total_amount;
            
            collections.total_amount = collections.cash + collections.cheque + collections.bank_deposit + collections.paypal;

        });

        res.data({
            data:           result,
            pagination:     pagination,
            collections:    collections
        }).send();

    }

    start ();

}

exports.manage = (req, res, next) => {

    function start () {

        mysql.use('master')
        .query(
            `
                SELECT  c.policy_no, s.insurance_quoted, s.assured_name,
                        s.total_premium, s.terms_of_payment, s.reference_quote_no,
                        s.classification, s.automated_partial_payment,
                        c.period_term_of_payment, c.date_received, c.cheque_issuing_bank,
                        c.cheque_date, c.mode_of_payment, c.amount_received,
                        c.cheque_branch, c.cheque_number, c.bd_date_deposited,
                        c.bd_bank_account, c.bd_reference_number, s.actual_balance,
                        s.actual_sum_total_amount, s.temporary_balance, s.temporary_sum_total_amount,
                        s.total_cancel_receipts
                FROM    crb_data c
                LEFT JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   c.crb_id = '${ req.params.crb_id }'
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving data Cash Receipts Bookd');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book record not found');

        res.item(result).send();

    }

    start ();

}

exports.retrieve_summary = (req, res, next) => {

    let per_page    = parseInt(req.query.per_page) || 10,
        page        = parseInt(req.query.page) || 1,
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination  = {},
        count;


    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        mode_of_payment     = req.query.mode_of_payment,
        bd_bank_account     = req.query.bd_bank_account,
        filter_date         = req.query.filter_date,
        search              = req.query.search,
        payee               = req.query.payee;

    let query = ``,
        collections = {},
        collection_amount;

    function start () {

        if (search){

            query += ` AND (s.assured_name LIKE '%${ search }'`;
            query += ` OR c.bd_reference_number LIKE '${ search }'`;
            query += ` OR c.cheque_number LIKE '${ search }'`;
            query += ` OR c.cheque_branch LIKE '${ search }')`;
 
        }

        if (filter_date) query += ` AND DATE(c.${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        
        if (mode_of_payment) query += ` AND c.mode_of_payment = '${ mode_of_payment }'`;

        if (bd_bank_account) query += ` AND c.bd_bank_account = '${ bd_bank_account }'`;
       
        if (payee) query += ` AND c.payee = '${ payee }'`; 
       
        mysql.use('master')
        .query(
            `
                SELECT  COUNT(c.crb_id) as counts
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'yes'
                ${ query }
            `,
            get_collections
        ).end();

    }

    function get_collections (err, result) {

        if (err) {
            winston.error('Error in selecting total counts for crb');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book records not found');

        count = result[0].counts;

        mysql.use('master')
        .query(
            `
                SELECT  c.mode_of_payment, sum(c.amount_received) as sub_total_amount
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'yes'
                ${ query }
                GROUP BY c.mode_of_payment
            `,
            get_data
        ).end();

    }

    function get_data (err, result) {

        if (err) {
            winston.error('Error in selecting total counts for crb');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book records not found');

        collection_amount = result;

        mysql.use('master')
        .query(
            `
                SELECT  c.date_use_for_filter, c.mode_of_payment,
                        c.amount_received, s.assured_name,
                        c.policy_no, c.period_term_of_payment,
                        c.is_deposited, c.cheque_issuing_bank, 
                        c.cheque_branch, c.cheque_number, c.cheque_date,
                        c.bd_date_deposited, c.bd_bank_account, c.bd_reference_number
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'yes'
                ${ query }
                ORDER BY s.sales_id DESC, s.date_created DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in selecting sales');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales not found');

        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        collections.cash = 0;
        collections.cheque = 0;
        collections.bank_deposit = 0;
        collections.total_amount = 0;
        collections.paypal = 0;

        _.each(collection_amount, (amount) => {

            if (amount.mode_of_payment == 'cash') collections.cash += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'cheque') collections.cheque += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'bank_deposit') collections.bank_deposit += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'paypal') collections.paypal += amount.sub_total_amount;
            
            collections.total_amount = collections.cash + collections.cheque + collections.bank_deposit + collections.paypal;

        });

        res.data({
            result,
            pagination,
            collections
        }).send();

    }

    start ();

}

exports.create_deposit = (req, res, next) => {

    const data = util._get
    .form_data({
        bd_date_deposited: '',
        bd_bank_account: '',
        bd_reference_number: '',
        temporary_balance: 1,
        temporary_sum_total_amount: 1,
        actual_sum_total_amount: 1,
        actual_balance: 1,
        total_cancel_receipts: 1,
        actual_fully_paid: '',
        sales_id: 1
    })
    .from(req.body);

    function start() {

        let crb = {
            bd_date_deposited: data.bd_date_deposited,
            bd_bank_account: data.bd_bank_account,
            bd_reference_number: data.bd_reference_number,
            is_deposited: 'yes',
            is_bounced: 'no'
        };

        mysql.use('master')
        .query(
            `
                UPDATE  crb_data
                SET ?
                WHERE   crb_id = '${ req.params.crb_id }'
            `,
            crb,
            update_sales
        ).end();

    }

    function update_sales(err, result) {

        if (err) {
            winston.error('Error in updating Cash Receipts Book');
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_UPDATED', 'No Cash Receipts Book was updated');
        
        let sales = {
            temporary_balance: data.temporary_balance,
            temporary_sum_total_amount: data.temporary_sum_total_amount,
            actual_sum_total_amount: data.actual_sum_total_amount,
            actual_balance: data.actual_balance,
            total_cancel_receipts: data.total_cancel_receipts,
            actual_fully_paid: data.actual_fully_paid
        };
        
        mysql.use('master')
        .query(
            `
                UPDATE  sales
                SET ?
                WHERE   sales_id = '${ data.sales_id }'
            `,
            sales,
            send_response
        ).end();

    }

    function send_response(err, result) {

        if (err) {
            winston.error('Error in updating sales');
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_UPDATED', 'No sales was updated');
        
        res.data({
            message: 'Deposit successful'
        }).send();

    }

  start();

}

exports.bank_balance = (req, res, next) => {

    const data = util._get
    .form_data({
        bank_policy_role: '',
        bd_bank_account: '',
        month: '',
        year: ''
    })
    .from(req.body);

    function start() {

        mysql.use('master')
        .query(
            `
                SELECT  *
                FROM    bank_balances
                WHERE   month = '${ data.month }'
                AND     year = '${ data.year }'
                AND     bank_policy_role = '${ data.bank_policy_role }'                                                                                                                                                                                               
                AND     bd_bank_Account = '${ data.bd_bank_account }'
            `,
            insert_bank_balance
        ).end();

    }

    function insert_bank_balance (err, result, args, last_query) {

        if (err) {
            winston.error('Error in retrieving bank balance', last_query);
            return next(err);
        }

        if (result.length) return res.error('NO_RECORD_CREATED', 'No bank balance record was created')

        mysql.use('master')
        .query(
            `
                INSERT INTO bank_balances
                SET ?
            `,
            data,
            send_response
        ).end();

    }

     function send_response (err, result, args, last_query) {

        if (err) {
            winston.error('Error in creating bank balance', last_query);
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_CREATED', 'No bank balance record was created');
        
        res.data({
            message: 'Successfully created new bank_balances',
            bank_balance_id:       result.insertId
        }).send();
    }

    start ();

}