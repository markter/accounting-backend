'use strict';

const mysql       = require('anytv-node-mysql');
const winston     = require('winston');
const jwt         = require('jsonwebtoken');
const util        = require(__dirname + '/../helpers/util');
const config      = require(__dirname + '/../config/config');
const crypto      = require(__dirname + '/../lib/cryptography');

exports.username = (req, res, next) => {

    const data = util._get
        .form_data({
            uname_email: ''
        })
        .from(req.body);

    function start () {

        if (data instanceof Error) return res.error('INC_DATA', data.message);

        data.uname_email = data.uname_email.toLowerCase();

        mysql.use('master')
        .query(
            `
                SELECT  id, username, email
                FROM    users
                WHERE   ( username = '${ data.uname_email }'
                          OR email = '${ data.uname_email }'
                        )
                AND     deleted IS NULL
            `,
            send_response
        ).end();

    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in logging in', last_query);
            return next(err);
        }

        if(!result.length) return res.error('LOG_FAIL', 'Invalid username or email');

        res.item(result)
        .send();

    }

    start();

}
 
exports.login = (req, res, next) => {
    const data = util._get
        .form_data({
            uname_email: '',
            password: ''
        })
        .from(req.body);

    function start () {

        if (data instanceof Error) return res.error('INC_DATA', data.message);

        data.uname_email = data.uname_email.toLowerCase();

        mysql.use('master')
            .query(
                `
                    SELECT  id, username,
                            IF(
                                UCASE(
                                    SHA1(
                                        CONCAT(MD5('${ data.password }'), '${ config.SALT }')
                                        )
                                ) = password, TRUE, FALSE
                            ) AS isPasswordValid,
                            email, fullname, agency_role, module_restriction,
                            permission_task
                    FROM    users
                    WHERE   (email LIKE '${ data.uname_email }' OR username LIKE '${ data.uname_email }')
                    AND     deleted IS NULL
                `,
                send_response
        ).end();

    }

    function send_response (err, result, args, last_query) {        
        let user,
            token,
            encrypted = {};

        if (err) {
            winston.error('Error in logging in', last_query);
            return next(err);
        }

        if (!result.length) return res.error('LOG_FAIL', 'Invalid username');
        
        if (!result[0].isPasswordValid) return res.error('LOG_FAIL', 'Incorrect Password');
        
        user = {
            id:                 result[0].id,
            username:           result[0].username,
            email:              result[0].email,
            agency_role:        result[0].agency_role,
            status:             result[0].status,
            date_created:       result[0].date_created,
            date_updated:       result[0].date_updated,
            module_restriction: result[0].module_restriction,
            permission_task:    result[0].permission_task
        };

        encrypted.user = crypto.encryptSyncNew(user);

        token = jwt.sign(encrypted, config.SECRET, {
                        algorithm: config.TOKEN_ALGO,
                        expiresIn: config.TOKEN_EXPIRATION
                    });

        user.token = token;

        req.redis.sadd(user.id.toString(), token);

        res.set('x-access-token', token)
           .item(user)
           .send();

    }

    start();
};

exports.logout = (req, res) => {
    const body  = req.body,
          token = body.token,
          id    = body.user.id.toString();

    function start () {
        if (token) {
            req.redis.srem(id, token);
            res.item({message: 'User successfully logged out'})
               .send();
        } else {
            res.error('NO_TOKEN', 'Please provide valid token in body form')
               .status(403)
               .send();
        }
    }

    start();
};

exports.verify_token = (req, res, next) => {
    const token = req.headers['x-access-token'],
          redis = req.redis;

    let decrypted,
        userId;

    function start () {

        if (token) {
            jwt.verify(token, config.SECRET, 
                  {algorithms : [config.TOKEN_ALGO]}, 
                  jwt_verify);
        }
        else {
            res.error('NO_TOKEN', 'Please provide valid token in body form')
               .status(403)
               .send();
        }

    }

    function jwt_verify (err, user) {
        if (err) {
            return res.status(404)
                      .error('UNAUTH', 'Failed to authenticate token.')
                      .send();
        }

        decrypted = crypto.decryptSyncNew(user.user);
        userId    = decrypted.id;

        redis.sismember(userId, token, 
                       send_response);


    }

    function send_response (err, isMember) {
        if (err || !isMember) {
            return res.status(404)
                      .error('UNAUTH', 'Failed to authenticate token.')
                      .send();
        }

        req.body.user  = decrypted;
        req.body.token = token;
        next();
    }

    start();

};