'use strict'

const mysql         = require('anytv-node-mysql');
const winston       = require('winston');
const _             = require('lodash');

const util          = require(__dirname + '/../helpers/util');
const data_export   = require(__dirname + '/../helpers/data_export');


exports.retrieve = (req, res, next) => {

    let per_page    = parseInt(req.query.per_page) || 10,
        page        = parseInt(req.query.page) || 1,
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination  = {},
        count;


    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        mode_of_payment     = req.query.mode_of_payment,
        bd_bank_account     = req.query.bd_bank_account,
        filter_date         = req.query.filter_date,
        search              = req.query.search,
        payee               = req.query.payee;

    let query = ``,
        collections = {},
        collection_amount;

    function start () {

        if (search){

            query += ` AND (s.assured_name LIKE '%${ search }'`;
            query += ` OR c.bd_reference_number LIKE '${ search }'`;
            query += ` OR c.cheque_number LIKE '${ search }'`;
            query += ` OR c.cheque_branch LIKE '${ search }')`;
 
        }

        if (filter_date) query += ` AND DATE(c.${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        
        if (mode_of_payment) query += ` AND c.mode_of_payment = '${ mode_of_payment }'`;

        if (bd_bank_account) query += ` AND c.bd_bank_account = '${ bd_bank_account }'`;
       
        if (payee) query += ` AND c.payee = '${ payee }'`; 
       
        mysql.use('master')
        .query(
            `
                SELECT  COUNT(c.crb_id) as counts
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'no'
                ${ query }
            `,
            get_collections
        ).end();

    }

    function get_collections (err, result) {

        if (err) {
            winston.error('Error in selecting total counts for crb');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book records not found');

        count = result[0].counts;

        mysql.use('master')
        .query(
            `
                SELECT  c.mode_of_payment, sum(c.amount_received) as sub_total_amount
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'no'
                ${ query }
                GROUP BY c.mode_of_payment
            `,
            get_data
        ).end();

    }

    function get_data (err, result) {

        if (err) {
            winston.error('Error in selecting total counts for crb');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Cash Receipts Book records not found');

        collection_amount = result;

        mysql.use('master')
        .query(
            `
                SELECT  c.crb_id, c.date_use_for_filter,
                        c.mode_of_payment, c.amount_received,
                        s.assured_name, c.policy_no, c.period_term_of_payment,
                        c.is_deposited, c.cheque_issuing_bank, 
                        c.cheque_branch, c.cheque_number, c.cheque_date,
                        c.bd_date_deposited, c.bd_bank_account, c.bd_reference_number
                FROM    crb_data c
                INNER JOIN sales s
                ON      c.policy_no = s.policy_no
                WHERE   1
                AND     c.is_deposited = 'no'
                ${ query }
                ORDER BY s.sales_id DESC, s.date_created DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in selecting sales');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales not found');

        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        collections.cash = 0;
        collections.cheque = 0;
        collections.bank_deposit = 0;
        collections.total_amount = 0;
        collections.paypal = 0;

        _.each(collection_amount, (amount) => {

            if (amount.mode_of_payment == 'cash') collections.cash += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'cheque') collections.cheque += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'bank_deposit') collections.bank_deposit += amount.sub_total_amount;
            
            if (amount.mode_of_payment == 'paypal') collections.paypal += amount.sub_total_amount;
            
            collections.total_amount = collections.cash + collections.cheque + collections.bank_deposit + collections.paypal;

        });

        res.data({
            result, collections, pagination
        }).send();

    }

    start ();

}

exports.update_crb = (req, res, next) => {

    const data = util._get
        .form_data({
            actual_balance: 1,
            actual_fully_paid: '',
            actual_sum_total_amount: 1,
            fully_paid: '', //! if account was fully paid ('yes', 'no')
            insurance_payment: 1, //! payment for insurance
            last_payment_date: '', //! last payment date
            last_period_term: 1, //! previous term of payment
            sales_remittance_type: '', //! net_remittance or gross_remittance
            temporary_balance: 1,
            temporary_sum_total_amount: 1,
            terms_of_payment: 1, //! payment terms
            total_cancel_receipts: 1,
            _payment_memo: '',
            _crb_notes: ''
        })
    .from(req.body);

    const crb = util._get
        .form_data({
            crb_data: [{
                crb_id: 1,
                period_term_of_payment: 1,
                policy_no: '',
                _amount_received: 1,
                _bd_bank_account: '',
                _bd_date_deposited: '',
                _bd_reference_number: '',
                _cheque_branch: '',
                _cheque_date: '',
                _cheque_issuing_bank: '',
                _cheque_number: '',
                _date_received: '',
                _date_use_for_filter: '',
                _direct_deposit: '',
                _is_deposit: '',
                _mode_of_payment: '',
                _payee: ''
            }]
        })
    .from(req.body);

    function start () {

        if (data instanceof Error) return res.error('INC_DATA', data.message);
        if (crb instanceof Error) return res.error('INC_DATA', crb.message);

        mysql.use('master')
        .query(
            `
                UPDATE  sales
                SET     ?
                WHERE   policy_no = '${ data.policy_no }'
            `,
            data,
            update_sales
        ).end();
        
    }
    
    function update_sales (err, result) {

        if (err) {
            winston.error('Error in update user', last_query);
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_UPDATED', 'No sales account was updated');

        _.each(crb, (crb_data) => {

            mysql.use('master')
            .query(
                `
                    UPDATE  crb_data
                    SET     ?
                    WHERE   crb_id = ${ crb_data.crb_id }
                `,
                crb_data,
                function (err, result) {
                    console.log(err);
                    
                }
            ).end();

        });
        
    }

    start ();

}