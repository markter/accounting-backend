'use strict'

const mysql     = require('anytv-node-mysql');
const winston   = require('winston');

/* const xls       = require(__dirname + '/../lib/export');
const data_export   = require(__dirname + '/../lib/export_columns'); */

const data_export   = require(__dirname + '/../helpers/data_export');

exports.retrieve = (req, res, next) => {

    //! DECLARATION - ISSUANCE VIEW PAGINATION
    let page            = parseInt(req.query.page || 0),
        perPage         = parseInt(req.query.per_page || 10),
        start_from      = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination      = {},
        count;

    //! DECLARATION - FILTERS
    let converted_to_sales  = req.query.converted_to_sales, 
        filter_date         = req.query.filter_date,
        filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        search              = req.query.search || '';

    let query               = ``;

    let exporting           = req.query.exporting,
        agency_name         = '';

    function start () {

        //! For Convert to Sales in Filter
        if (converted_to_sales == 0 || converted_to_sales == 1) query += ' and converted_to_sales=' + mysql.escape(converted_to_sales);
        //! For Date (FROM and TO) Filter
        if (filter_date) query += ' AND DATE(' + filter_date +') BETWEEN '+ mysql.escape(filter_date_from) +' AND ' + mysql.escape(filter_date_to);
        //! Add SEARCH
        if(search) {
            query += ' and (name LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR reference_number LIKE ' + mysql.escape('%'+search+'%');
            query += ' )';   
        }
        
        //! If not export, go to count issuance function
        if(!exporting) count_issuance();

    }

    function count_issuance () {

        //! Count all data of issuance with filters,
        mysql.use('quote')
        .query(
            `
                SELECT  COUNT(*) as TotalData 
                FROM    issuance
                WHERE 1 
                ${query}
                AND     deleted IS NULL
            `,
            issuance_data
        )
        .end();

    }

    function issuance_data (err, result, args, last_query) {
        //! If has error, display error message
        if (err) {
            winston.error('Error in issuance pagination', last_query);
            return next(err);
        }
        //! If there is no data, display 'No results found'
        if (!result.length) return res.error('ZERO_RES', 'No results found');

        //! Save the total count in count variable
        count = result;
    
        //! Retrieve all issuance with filters and pagination
        mysql.use('quote')
        .query(
            `
                SELECT  issuance_id, reference_number, name,
                        car_insurance_for, email, date_created,
                        address, post_code, mobile, phone1,
                        insurance_company, mode_of_delivery,
                        effectivity_date
                FROM    issuance 
                WHERE 1
                ${ query } 
                AND     deleted IS NULL 
                ORDER BY date_created DESC 
                LIMIT   ${ start_from }, ${ perPage }
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result) {
        //! If error, display errror message
        if (err) {
            winston.error('Error in retrieving subscribers');
            return next(err);
        }
        //! If has no result, display 'No results found'
        if (!result.length) return res.error('ZERO_RES', 'No results found');

        //! Save the total count as TotalData
        pagination.TotalData = count[0].TotalData;
        //! Get the total number of pages
        pagination.TotalPage = Math.ceil(pagination.TotalData / perPage);
        
        //! Display all data and page info
        res.data({result:result, pagination:pagination})
        .send();

    }

    start ();

}

exports.retrieve_id = (req, res, next) => {

    let data;

    function start () {

        mysql.use('quote')
        .query(
            `
                SELECT  issuance_id, name, address,
                        post_code, email, mobile, phone1,
                        car_insurance_for, vin_chassi_no,
                        motor_engine_no, color,
                        plate_conduction_no, fair_market_value,
                        classification, mortgagee, insurance_company,
                        acts_of_god_included, is_mortgage,
                        bodily_injury_property_damage,
                        personal_accident_amount, rate, 
                        acts_of_god_rate, fair_market_value_rate,
                        bodily_injury_premium, property_damage_premium,
                        bi_pd_premium, pa_premium, aon_premium, 
                        od_premium, od_aon_premium, annual_premiums,
                        lto_interconnectivity, docstamps, evat,
                        lgt, total_premiums, effectivity_date,
                        memo, user, agent_sales_person,
                        date_created, reference_number, delivery_date,
                        accessories_memo, payment_memo, category, location, 
                        template, promotions, mode_of_delivery, providerCode
                FROM    issuance
                WHERE   issuance_id = '${ req.params.id }'
                AND     deleted IS NULL
                AND     converted_to_sales = 0
            `,
            generate_commission
        ).end();

    }

    function generate_commission (err, result, args, last_query) {

        //! If error, display errror message
        if (err) {
            winston.error('Error in retrieving subscribers');
            return next(err);
        }
        //! If has no result, display 'No results found'
        if (!result.length) return res.error('ZERO_RES', 'No results found');

        data = result[0];

        if (data.insurance_company == "Mercantile Insurance" && data.classification == "PC" && data.category == "Motor/Car") {
            data.od_theft_comm       = (data.od_premium - (data.fair_market_value * 0.009)) * 1.247;
            data.aon_comm            = data.aon_premium * 0.20;
            data.bi_pd_comm          = data.bi_pd_premium * 0.25;
            data.total_commission    = data.od_theft_comm + data.aon_comm + data.bi_pd_comm;
            
            send_response();
        }

        if (data.insurance_company == 'Mercantile Insurance' && data.classification == 'CV' && data.category == "Motor/Car") {
            data.od_theft_comm       = (data.od_premium - (data.fair_market_value * 0.007)) * 1.247;
            data.aon_comm            = data.aon_premium * 0.20;
            data.bi_pd_comm          = data.bi_pd_premium * 0.25;
            data.total_commission    = data.od_theft_comm + data.aon_comm + data.bi_pd_comm;

            send_response();
        }

        if (data.insurance_company == 'Standard Insurance' && data.classification == 'PC' && data.category == "Motor/Car") {
            data.od_theft_comm       = (data.od_premium - (data.fair_market_value * 0.013)) * 1.247;
            data.aon_comm            = (data.aon_premium - (data.fair_market_value * 0.0025)) * 1.247;
            data.bi_pd_comm          = data.bi_pd_premium * 0.25;
            data.total_commission    = data.od_theft_comm + data.aon_comm + data.bi_pd_comm;

            send_response();
        }

        if (data.insurance_company == 'Standard Insurance' && data.classification == 'CV' && data.category == "Motor/Car") {
            data.od_theft_comm      = (data.od_premium - (data.fair_market_value * 0.008)) * 1.247;
            data.aon_comm           = (data.aon_premium - (data.fair_market_value * 0.0025)) * 1.247;
            data.bi_pd_comm         = data.bi_pd_premium * 0.25;
            data.total_commission   = data.od_theft_comm + data.aon_comm + data.bi_pd_comm;

            send_response();
        }

        if (data.insurance_company == 'FPG Insurance' && data.category == "Motor/Car") {
            data.total_commission = data.gross_premium * 0.25;
            send_response();
        }

        if (data.insurance_company == 'Mercantile Insurance' && (data.classification == 'PC' || data.classification == 'CV') && (data.category == 'ECTPL' || data.category == 'ECTPL Website')) {
            data.total_commission = data.total_premium - 270;
            send_response();
        }

        if (data.insurance_company == 'Mercantile Insurance' && (data.category == 'ECTPL' || data.category == 'ECTPL Website') && data.classification == 'Motorcycle') {
            data.total_commission = data.total_premium - 180;
            send_response();
        }

        if (data.insurance_company == 'Mercantile Insurance' && (data.category == 'ECTPL' || data.category == 'ECTPL Website') && data.classification == 'Truck') {
            data.total_commission = tdata.total_premium - 650;
            send_response();
        }

        if (data.insurance_company == 'Standard Insurance' && (data.category == 'ECTPL' || data.category == 'ECTPL Website')) {
            data.total_commission = data.gross_premium * 0.35;
            send_response();
        }

    } 

    function send_response () {

        res.data({data})
        .send();

    }

    start ();
    
}


exports.export_data = (req, res, next) => {
    console.log('here')
    //! DECLARATION - FILTERS
    let converted_to_sales  = req.query.converted_to_sales, 
        filter_date         = req.query.filter_date,
        filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        search              = req.query.search || '';

    let query               = ``;

    let issuance_data;

    function start () {

        //! For Convert to Sales in Filter
        if (converted_to_sales == 0 || converted_to_sales == 1) query += ' and converted_to_sales=' + mysql.escape(converted_to_sales);
        //! For Date (FROM and TO) Filter
        if (filter_date) query += ' AND DATE(' + filter_date +') BETWEEN '+ mysql.escape(filter_date_from) +' AND ' + mysql.escape(filter_date_to);
        //! Add SEARCH
        if(search) {
            query += ' and (name LIKE ' + mysql.escape('%'+search+'%');
            query += ' OR reference_number LIKE ' + mysql.escape('%'+search+'%');
            query += ' )';   
        }
        
        //! Retrieve all issuance with filters
        mysql.use('quote')
        .query(
            `
                SELECT  *
                FROM    issuance 
                WHERE 1
                ${ query } 
                AND     deleted IS NULL 
                ORDER BY date_created DESC 
            `,
            to_export
        )
        .end();

    }

    function to_export (err, result) {
        //! If Error, diplay error message
        if (err) {
            winston.error('Error in retrieving sales');
            return next(err);
        }
        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');
        //! Store retrieved data
        issuance_data = result;
        //! Put the stored data in req.body
        req.body.issuance_data = issuance_data;
        //! Redirect to sales export library
        data_export.issuance_export(req, res, next);

    }

    start ();

}