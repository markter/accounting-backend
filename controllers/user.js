'use strict';

const mysql             = require('anytv-node-mysql');
const winston           = require('winston');
const { v4: uuidv4 }    = require('uuid');

const util              = require(__dirname + '/../helpers/util');
const config            = require(__dirname + '/../config/config');


exports.create = (req, res, next) => {
    const data = util._get
        .form_data({
            username: '',
            email: '',
            password: '',
            fullname: '',
            agency_role: '',
            module_restriction: '',
            permission_task: ''
        })
        .from(req.body);

    function start() {

        if (data instanceof Error) return res.error('INC_DATA', data.message);
        
        mysql.use('master')
        .query (
            `
                SELECT  *
                FROM    users
                WHERE   email = '${ data.email }'
                OR      username = '${ data.username }'
            `,
             create_user
        )
        .end();
    }

    function create_user (err, result, args, last_query) {
        if (err) {
            winston.error('Error in getting user', err, last_query);
            return next(err);
        }

        if (result.length) return res.error('INVALID_EMAIL_UNAME', 'Email or Username is already in use');

        data.id = uuidv4();

        mysql.use('master')
        .query(
            `
                INSERT INTO 
                        users (
                            id, username, email, password,
                            fullname, agency_role,
                            module_restriction, permission_task
                        )
                VALUES  (
                            '${ data.id }', '${ data.username }', '${ data.email }',
                            UCASE(SHA1(CONCAT(MD5('${ data.password }'), '${config.SALT}'))), 
                            '${ data.fullname }', '${ data.agency_role }',
                            '${ data.module_restriction }', '${ data.permission_task }'
                        )
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in creating user', last_query);
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_CREATED', 'No user was created');

        res.data({
            message:    'Successfully created user',
            id:         data.id,
            username:   data.username,
            email:      data.email,
            fullName:   data.fullname
        })
        .send();
    }

    start();
};

exports.retrieve = (req, res, next) => {
    function start () {
        mysql.use('master')
        .query(
            `
                SELECT  id, username, email,
                        fullname, agency_role,
                        status, module_restriction,
                        permission_task, date_created
                FROM    users
                WHERE   deleted IS NULL
            `,
            send_response
        )
        .end();
    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in retrieving users', last_query);
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'User not found')
        
        res.items(result)
        .send();
    }

    start();
};

exports.retrieve_id = (req, res, next) => {
    function start () {
        mysql.use('master')
        .query(
            `
                SELECT  *
                FROM    users
                WHERE   id = '${ req.params.id }'
                AND     deleted IS NULL
                LIMIT   1
            `,
            send_response
        )
        .end();
    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in retrieving users', last_query);
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'User not found')
        
        res.data(result[0])
            .send();
    }

    start();
};

 exports.update = (req, res, next) => {
    const data = util._get
        .form_data({
            email: '',
            _fullname: ''
        })
        .from(req.body);

    function start() {

        if (data instanceof Error) return res.error('INC_DATA', data.message);

        data.date_updated = new Date();

        mysql.use('master')
        .query(
            `
                SELECT  *
                FROM    users
                WHERE   id != '${ req.params.id }'
                AND     email = '${ data.email }'
            `,
            update_user
        )
        .end();
    }

    function update_user (err, result, args, last_query) {
        if (err) {
            winston.error('Error in getting user', err, last_query);
            return next(err);
        }

        if (result.length) return res.error('INVALID_EMAIL_UNAME', 'Email is already in use');
        
        mysql.use('master')
        .query(
            `
                UPDATE  users
                SET     ?
                WHERE   id = '${ req.params.id }'
            `,
            data,
            send_response
        )
        .end();

    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in update user', last_query);
            return next(err);
        }

        if (!result.affectedRows) return res.error('NO_RECORD_UPDATED', 'No user was updated');

        data.id = req.params.id;

        res.data(data)
           .send();
    }

    start();
};

exports.delete = (req, res, next) => {

    function start () {

        mysql.use('master')
        .query(
            `
                UPDATE  users
                SET     deleted = NOW()
                WHERE   deleted IS NULL
                AND     id = '${ req.params.id }'
            `,
            send_response
        )
        .end();
    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in retrieving user', last_query);
            return next(err);
        }

        if (result.affectedRows === 0) return res.error('NO_RECORD_DELETED', 'No User was deleted');
        
        res.item({message: 'Successfully deleted user'})
           .send();
    }

    start();

};

exports.change_password = (req, res, next) => {
    const body  = req.body,
          redis = req.redis,
          id    = body.user.id,
          data  = util._get
                    .form_data({
                        currentPassword: '',
                        newPassword: '',
                        _confirmPassword: ''
                    })
                    .from(req.body);
          

    function start () {

        if (data instanceof Error) return res.error('INC_DATA', data.message);

        if (data.confirmPassword && data.newPassword !== data.confirmPassword)
            return res.error('INV_PASS', 'Invalid password confirmation');

        mysql.use('master')
        .query(
            `
                UPDATE  users
                SET     password = UCASE(SHA1(CONCAT(MD5('${ data.newPassword }'), '${config.SALT}'))),
                        date_updated = NOW()
                WHERE   password = UCASE(SHA1(CONCAT(MD5('${ data.currentPassword }'), '${config.SALT}')))
                AND     id = '${ id }'
                LIMIT 1
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result, args, last_query) {
        if (err) {
            winston.error('Error in retrieving user', last_query);
            return next(err);
        }

        if (result.affectedRows === 0) return res.error('NO_PASS', 'Please check current password');
        

        // Delete all active tokens
        // and remain the current one
        redis.del(id.toString());
        redis.sadd(id.toString(), body.token);

        res.item({message: 'Password successfully updated'})
           .send();
    }

    start();
};