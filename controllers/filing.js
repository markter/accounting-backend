const mysql     = require('anytv-node-mysql');
const winston   = require('winston');

const util      = require(__dirname + '/../helpers/util');

exports.retrieve = (req, res, next) => {

    let page                = parseInt(req.query.page || 0),
        perPage             = parseInt(req.query.per_page || 10),
        start_from          = (page - 1) < 0 ? 0 : (page -1)  * perPage,
        query               = '',
        count;

    let filter_crb          = req.query.search,
        effective_date      = req.query.effectivity_date;
       
    function start () {

        if (filter_crb){

            query += ' AND (c.policy_no LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' OR c.bd_date_deposited LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' OR s.assured_name LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' OR s.mortgagee LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ' OR s.reference_quote_no LIKE' + mysql.escape('%'+filter_crb+'%');
            query += ')';
 
        }

        if (effective_date) query += ' AND s.effectivity_date = ' +mysql.escape(effective_date);
        
        mysql.use('master')
        .query(
            `
                SELECT  COUNT(*) AS count
                FROM    sales s
                LEFT JOIN crb_data c
                ON      s.sales_id = c.sales_id
                LEFT JOIN bank_filing b
                ON      s.sales_id = b.sales_id
                WHERE   c.period_term_of_payment = '1'
                AND     (
                            s.mortgagee <> 'NIL' 
                            AND s.mortgagee <> 'NILL' 
                            AND s.mortgagee <> 'NONE' 
                            AND s.mortgagee <> 'non' 
                            AND s.mortgagee <> 'NON FINANCING' 
                            AND s.mortgagee <> 'NONFINANCING' 
                            AND s.mortgagee IS NOT NULL
                        )
                AND     date_received IS NOT NULL
                ${ query }
            `,
            data
        )
        .end();

    }

    function data (err, result, args, last_query) {

        if (err) {
            winston.error('Error in total counts of initial payments', last_query);
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Records for bank filing not found');

        count = result[0].count;

        mysql.use('master')
        .query(
            `
                SELECT      s.sales_id, s.assured_name, c.policy_no,
                            c.date_received, s.reference_quote_no,
                            s.mortgagee, b.status, b.rider, b.date_filed,
                            s.is_filed, s.effectivity_date, b.notes
                FROM        sales s
                LEFT JOIN   crb_data c
                ON          s.sales_id = c.sales_id
                LEFT JOIN   bank_filing b
                ON          s.sales_id = b.sales_id
                WHERE       c.period_term_of_payment = '1'
                AND         (
                                s.mortgagee <> 'NIL' 
                                AND s.mortgagee <> 'NILL' 
                                AND s.mortgagee <> 'NONE' 
                                AND s.mortgagee <> 'non' 
                                AND s.mortgagee <> 'NON FINANCING' 
                                AND s.mortgagee <> 'NONFINANCING' 
                                AND s.mortgagee IS NOT NULL
                            )
                AND         date_received IS NOT NULL
                ${ query }
                ORDER BY    c.sales_id DESC
                LIMIT       ${ start_from }, ${ perPage }
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result, args, last_query) {

        if (err) {
            winston.error('Error in retrieving bank filing records', last_query);
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Records for bank filing not found');

        let pagination = {};

        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / perPage);

        res.data({ result, pagination: pagination })
        .send();

    }

    start ();

}

exports.retrieve_id = (req, res, next) => {

    function start () {

        mysql.use('master')
        .query(
            `
                SELECT  id, sales_id, status,
                        rider, date_filed, notes
                FROM    bank_filing
                WHERE   sales_id = '${ req.params.sales_id }'
            `,
        send_response
        )
        .end();

    }

    function send_response (err, result, args, last_query) {

        if (err) {
            winston.error('Error in retrieving bank filing record', last_query);
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'Bank filing record not found');

        res.data(result[0])
        .send();

    }

    start();

}