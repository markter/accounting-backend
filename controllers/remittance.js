'use strict'

const mysql         = require('anytv-node-mysql');
const winston       = require('winston');
const _             = require('lodash');

const util          = require(__dirname + '/../helpers/util');
const data_export   = require(__dirname + '/../helpers/data_export');


exports.retrieve_record = (req, res, next) => {

    let page        = parseInt(req.query.page) || 1,
        per_page    = parseInt(req.query.per_page) || 10,
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination  = {},
        count;

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        insurance_quoted    = req.query.insurance_quoted,
        remittance_type     = req.query.remittance_type,
        filter_date         = req.query.filter_date,
        search              = req.query.search;

    let query = ``;

    function start () {

         if (search) {
            query += ` AND (remittance_type LIKE '%${ search }%'`;
            query += ` OR remittance_ref_no LIKE '%${ search }%'`;
            query += ` OR cheque_account_name LIKE '%${ search }%'`;
            query += ` OR cheque_number LIKE '%${ search }%'`;
            query += ` OR cheque_date LIKE '%${ search }%')`;
        }

        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
                
        if (remittance_type) query += ` AND remittance_type = '${ remittance_type }'`;
        
        if (insurance_quoted) query += ` AND insurance_quoted = '${ insurance_quoted }'`;
        
        mysql.use('master')
        .query(
            `
                SELECT COUNT(*) AS counts
                FROM    remittance_record
                WHERE   1
                ${ query }
            `,
            retrieve_data
        ).end();

    }

    function retrieve_data (err, result) {

        if (err) {
            winston.error('Error in retrieving remittance record');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Remittance Record not found');
        
        count = result[0].counts;

        mysql.use('master')
        .query(
            `
                SELECT  remittance_record_id, remittance_ref_no,
                        remittance_type, date_of_remittance, already_process,
                        insurance_name, insurance_quoted, cheque_account_name,
                        cheque_number, cheque_date
                FROM    remittance_record
                WHERE   1
                ${ query }
                ORDER BY remittance_record_id DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving remittance record');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Remittance Record not found');
        
        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        res.data({result:result, pagination:pagination})
        .send();

    }

    start ();

}


exports.retrieve_policy = (req, res, next) => {

    let page        = parseInt(req.query.page) || 1,
        per_page    = parseInt(req.query.per_page) || 10,
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination  = {},
        count;

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        filter_date         = req.query.filter_date;

    let sales_remittance_type   = req.query.sales_remittance_type,
        is_gross_remitted       = req.query.is_gross_remitted,
        insurance_quoted        = req.query.insurance_quoted,
        is_net_remitted         = req.query.is_net_remitted,
        paid_status             = req.query.paid_status,
        is_cancel               = req.query.is_cancel,
        search                  = req.query.search;
    
    let query = ``;

    function start () {

        if (search) {
            query += ` AND (s.policy_no LIKE '%${ search }%'`;
            query += ` OR s.reference_quote_no LIKE '%${ search }%'`;
            query += ` OR s.assured_name LIKE '%${ search }%'`;
            query += ` OR s.last_payment_date LIKE '%${ search }%')`;
        }

        if (is_cancel) query += ` AND s.is_cancel = '${ is_cancel }'`;
        
        if (filter_date) query += ` AND DATE(s.${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;

        if (is_gross_remitted) query += ` AND s.is_gross_remitted = '${ is_gross_remitted }'`;
        
        if (insurance_quoted) query += ` AND s.insurance_quoted = '${ insurance_quoted }'`;
        
        if (paid_status == 'fully_paid') query += ` AND s.fully_paid = 'yes'`;

        if (paid_status == 'unpaid') query += ` AND s.temporary_balance = s.total_premium AND s.fully_paid = 'no'`;

        if (paid_status == 'partially_paid') query += ` AND s.temporary_balance != s.total_premium AND s.fully_paid = 'no'`;
        
        if (sales_remittance_type == 'net_remittance' || sales_remittance_type == 'gross_remittance')
            query += ` AND s.sales_remittance_type LIKE '%${ sales_remittance_type }%'`;

        if (sales_remittance_type == 'net_gross_remittance') 
            query += ` AND (s.sales_remittance_type LIKE '%net_remittance%' OR s.sales_remittance_type LIKE '%gross_remittance%')`;

        if (is_net_remitted) {

            (is_net_remitted == 'no_and_partial') 
            ? query += ` AND s.is_net_remitted in ('no', 'partial')`
            : query += ` AND s.is_net_remitted = '${ is_net_remitted }'`;

        }

        mysql.use('master')
        .query(
            `
                SELECT COUNT(s.sales_id) AS counts
                FROM    sales s
                WHERE   1
                ${ query }
            `,
            retrieve_data
        ).end();

    }

    function retrieve_data (err, result) {

        if (err) {
            winston.error('Error in retrieving remittance record');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales policy not found');

        count = result[0].counts;
 
        mysql.use('master')
        .query(
            `
                SELECT  s.policy_no, s.insurance_name, s.assured_name, rr.remittance_type,
                        s.actual_balance, s.is_net_remitted, s.is_gross_remitted,
                        rr.date_of_remittance, s.gross_premium, s.total_premium,
                        s.issuance_date, s.effectivity_date, s.date_created
                FROM    sales s
                LEFT JOIN remittance_payment rp
                ON      s.policy_no = rp.policy_no
                LEFT JOIN remittance_record rr
                ON      rp.remittance_record_id = rr.remittance_record_id
                WHERE   1
                ${ query }
                ORDER BY s.sales_id DESC, s.date_created DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving sales policy');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales not found');
        
        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        res.data({data:result, pagination:pagination})
        .send();

    }

    start ();

}

exports.retrieve_pool = (req, res, next) => {

    let page        = parseInt(req.query.page) || 1,
        per_page    = parseInt(req.query.per_page) || 10,
        start_from  = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        pagination  = {},
        count;

    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        filter_date         = req.query.filter_date;

    let sales_remittance_type   = req.query.sales_remittance_type,
        is_gross_remitted       = req.query.is_gross_remitted,
        insurance_quoted        = req.query.insurance_quoted,
        is_net_remitted         = req.query.is_net_remitted,
        is_cancel               = req.query.is_cancel,
        search                  = req.query.search;

    let query = ``;

    function start () {

        if (search) {
            query += ` AND (policy_no LIKE '%${ search }%'`;
            query += ` OR assured_name LIKE '%${ search }%'`;
            query += ` OR car_make_model LIKE '%${ search }%')`;
        }

        if (is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;

        if (insurance_quoted) query += ` AND insurance_quoted = '${ insurance_quoted }'`;

        if (is_gross_remitted) query += ` AND is_gross_remitted = '${ is_gross_remitted }'`;

        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        
        if (sales_remittance_type == 'net_remittance' || sales_remittance_type == 'gross_remittance')
            query += ` AND sales_remittance_type LIKE '%${ sales_remittance_type }%'`;

        if (is_net_remitted) {

            (is_net_remitted == 'no_and_partial') 
            ? query += ` AND is_net_remitted in ('no', 'partial')`
            : query += ` AND is_net_remitted = '${ is_net_remitted }'`;

        }
        
        mysql.use('master')
        .query(
            `
                SELECT COUNT(sales_id) AS counts
                FROM    sales
                WHERE   actual_fully_paid = 'yes'
                ${ query }
            `,
            retrieve_data
        ).end();

    }

    function retrieve_data (err, result) {

        if (err) {
            winston.error('Error in retrieving remittance record');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales policy not found');

        count = result[0].counts;

        mysql.use('master')
        .query(
            `
                SELECT  policy_no, assured_name, car_make_model,
                        email_address, effectivity_date, last_payment_date
                FROM    sales
                WHERE   actual_fully_paid = 'yes'
                ${ query }
                ORDER BY sales_id DESC, date_created DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

        if (err) {
            winston.error('Error in retrieving sales policy');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Sales policy not found');
        
        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        res.data({result:result, pagination:pagination})
        .send();

    }

    start ();

}

exports.create_record = (req, res, next) => {

    const data = util._get
    .form_data({
        date_of_remittance: '',
        insurance_name: '',
        insurance_quoted: '',
        policy_role: '',
        remittance_ref_no: '',
        remittance_type: '',
        _cheque_account_name: '',
        _cheque_date: '',
        _cheque_number: ''
    })
    .from(req.body);

    function start() {

        mysql.use('master')
        .query(
            `
                SELECT  remittance_record_id
                FROM    remittance_record
                WHERE   remittance_ref_no = '${ data.remittance_ref_no }'
                AND     insurance_quoted = '${ data.insurance_quoted }'
            `,
            add_record
        ).end();

    }

    function add_record (err, result) {

        if (err) {
            winston.error('Error in verifying remittance record');
            return next(err);
        }

        if (result.length) return res.error('INVALID_REMITTANCE_REF_NO', 'Reference number is already in use');
        
        mysql.use('master')
        .query(
            `
                INSERT INTO remittance_record
                SET ?
            `,
            data,
            send_response
        ).end();

    }


    function send_response (err, result) {

        if (err) {
            winston.error('Error in creating remittance_record');
            return next(err);
        }

        if (!result.affectedRows)  return res.error('NO_RECORD_CREATED', 'No remittance record was created');
        
        res.data({
            message:            'Successfully created remittance record',
            id:                 result.insertId,
            insurance_name:     data.insurance_name,
            insurance_quoted:   data.insurance_quoted,
            remittance_ref_no:  data.remittance_ref_no
        }).send();

    }

    start();

}

exports.export_data = (req, res, next) => {

    //! Filters
    let filter_date_from        = req.query.filter_date_from || '1190-01-01',
        filter_date_to          = req.query.filter_date_to || '2993-12-31',
        search                  = req.query.search,
        filter_date             = req.query.filter_date,
        paid_status             = req.query.paid_status,
        is_cancel               = req.query.is_cancel,
        insurance_quoted        = req.query.insurance_quoted,
        is_net_remitted         = req.query.is_net_remitted,
        is_gross_remitted       = req.query.is_gross_remitted,
        sales_remittance_type   = req.query.sales_remittance_type;

    let query = '',
        sales_data,
        remittance_data,
        gross_remittance_data;

    function start () {

        if (search) {
            query += ` AND (policy_no LIKE '%${ search }%'`;
            query += ` OR reference_quote_no LIKE '${ search }'`;
            query += ` OR assured_name LIKE '${ search }'`;
            query += ` OR last_payment_date LIKE '${ search }')`;
        }

        if (is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;

        if (filter_date) query += ` AND DATE (${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;

        if (sales_remittance_type && (sales_remittance_type == 'net_remittance' || sales_remittance_type == 'gross_remittance'))
            query += ` AND sales_remittance_type LIKE '${ sales_remittance_type }'`;

        if (sales_remittance_type && sales_remittance_type == 'net_gross_remittance')
            query += ` AND (sales_remittance_type LIKE '%net_remittance%' OR sales_remittance_type LIKE '%gross_remittance%'`;  
        
        if (is_net_remitted && is_net_remitted == 'no_and_partial') query += ` `;

        if (is_net_remitted) {

            (is_net_remitted == 'no_and_partial')
            ? query += ` AND is_net_remitted in ('no', 'partial')`
            : query += ` AND is_net_remitted = '${ is_net_remitted }'`;

        }

        if (is_gross_remitted) query += ` AND is_gross_remitted = '${ is_gross_remitted }'`;

        if (insurance_quoted) query += ` AND insurance_quoted = '${ insurance_quoted }'`;

        if (paid_status && paid_status == 'fully_paid') query += ` AND fully_paid = 'yes'`;
        if (paid_status && paid_status == 'unpaid') query += ` AND temporary_balance = total_premium AND fully_paid = 'no'`;
        if (paid_status && paid_status == 'partially_paid') query += ` AND temporary_balance!= total_premium  AND fully_paid = 'no'`;

        mysql.use('master')
        .query(
            `
                SELECT  *, 
                        CONCAT(classification,'-',insurance_quoted) as cv_pc
                FROM    sales 
                WHERE   1
                ${ query }
                ORDER BY sales_id DESC, date_created DESC
            `,
            get_remittance
        ).end();

    }

    function get_remittance (err, result) {

        if (err) console.log(err);
        
        sales_data = result;
        
        mysql.use('master')
        .query(
            `
                SELECT  *
                FROM    remittance_record rr
                INNER JOIN remittance_payment rp
                ON      rr.remittance_record_id = rp.remittance_record_id
            `,
            get_gross_remittance
        ).end();

    }

    function get_gross_remittance (err, result) {

        if (err) console.log(err);

        remittance_data = result;

        mysql.use('master')
        .query(
            `
                SELECT  *
                FROM    remittance_record rr
                INNER JOIN gross_remittance_payment grp
                ON      rr.remittance_record_id = grp.remittance_record_id
            `,
            to_export
        ).end();
    }

    function to_export (err, result) {
        //! If Error, diplay error message
        if (err) {
            winston.error('Error in retrieving sales');
            return next(err);
        }
        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');
        //! Store retrieved data
        gross_remittance_data = result;
        //! Put the stored data in req.body
        req.body.sales_data = sales_data;
        req.body.remittance_data = remittance_data;
        req.body.gross_remittance_data = gross_remittance_data;
        req.body.insurance_quoted = insurance_quoted;
        //! Redirect to sales export library
        data_export.remittance_export(req, res, next);

    }

    start();

}