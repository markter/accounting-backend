'use strict'

const mysql     = require('anytv-node-mysql');
const winston   = require('winston');

exports.category = (req, res, next) => {

    // let username = req.body.userObj.username,
    //     q_id;

    function start (queryId) {

        // q_id = queryId;
        get_data ();

    }

    function get_data () {

        mysql.use('quote')
        .query(
            `
                SELECT  id, category
                FROM    category
                WHERE   deleted IS NULL
            `,
            send_response
        );

    }

    function send_response (err, result, args, last_query) {

        if (err) {
            winston.error('Error in retrieving category', last_query);
            return next(err);
        }

        if (result.length == 0) return res.error('ZERO_RES', 'No results found');

        res.items(result)
        .send();

        /* q_id.then((value) => {
            logger.log_end(value, `${username} successfully retrieve categories`)
        }) */

    }

    // start (logger.log(req));
    start();

}