'use strict'

const mysql         = require('anytv-node-mysql');
const winston       = require('winston');
const _             = require('lodash');

const util          = require(__dirname + '/../helpers/util');
const data_export   = require(__dirname + '/../helpers/data_export');


exports.create = (req, res, next) => {
    //! DATA VALIDATION
    const data = util._get
        .form_data({
            actual_balance: 1, //! Same Total Premium
            actual_fully_paid: '',
            address: '',
            agency_user: 1,
            agent_sales_person: '',
            aon_comm: 1,
            aon_premium: 1,
            assured_name: '',
            automated_partial_payment: 1, //! total_premium / terms_of_payment
            bi_pd_comm: 1,
            bi_pd_premium: 1,
            category: '',
            car_make_model: '',
            classification: '',
            contact_no: '',
            contact_no2: '',
            delivery_address: '',
            docstamps: 1,
            effectivity_date: '',
            email_address: '',
            evat: 1,
            fmv: 1,
            fully_paid: '',
            gross_premium: 1,
            insurance_name: '',
            insurance_quoted: '',
            issuance_date: '',
            issued_on: '',
            is_mortgage: '',
            lgt: 1,
            lto_interconnectivity: 1,
            mode_of_delivery: '',
            _mortgagee: '',
            od_premium: 1,
            od_theft_comm: 1,
            pa_premium: 1,
            _payment_memo: '',
            policy_no: '',
            policy_role: '',
            promotions: '',
            reference_quote_no: '',
            salespolicy_notes: '',
            template: '',
            temporary_balance: 1, //! total premium
            terms_of_payment: 1,
            total_comm_compute: 1, //automated comm computation
            total_premium: 1,
        })
        .from(req.body);

    var terms_changed = req.body.terms_changed;
    delete req.body.terms_changed;

    let query = ``;

    //! FOR GENERATE CRB
    let crb_query = ``,
        crb_array = [],
        crb_terms;

    function start() {
        if (data instanceof Error) return res.error('INC_DATA', data.message);

        //! ADD THIS QUERY IF MOD IS DELIVER & COLLECT
        if (data.mode_of_delivery == 'Deliver & Collect') query += ` , is_collected = 1`;

        //! ADD ALL DATA INTO SALES TABLE
        mysql.use('master')
        .query(
            `
                INSERT INTO sales
                SET ?
                ${ query }
            `,
            data,
            insert_crb
        )
        .end();

    }

    function insert_crb (err, result){
        //! ERR MSG IN CREATING SALES
        if (err) {
            winston.error('Error in creating sales');
            return next(err);
        }

        //! DISPLAY ERR MSG IF NO RECORD CREATED
        if (!result.affectedRows) return res.error('NO_RECORD_CREATED', 'No record was created');
        
        //! GENERATE PAYMENT TERM
        crb_terms = _.range(1, (data.terms_of_payment + 1));
        //! GENERATE QUERY SCRIPT depends on terms of payment
        crb_generate = _.forEach(crb_terms, (value) => {

            if (value != data.terms_of_payment) crb_query = `(${ result.insertId }, '${ data.policy_no }', ${ value }, ${ data.automated_partial_payment })`;
            if (value == data.terms_of_payment) crb_query = `(${ result.insertId }, '${ data.policy_no }', ${ value }, ${ data.automated_partial_payment })`;
            crb_array.push(crb_query);

        });

        //! ADD GENERATED CRB IN TABLE 
        mysql.use('master')
        .query(
            `
                INSERT INTO crb_data 
                            (sales_id, policy_no, period_term_of_payment, amount_to_pay)
                VALUES ${ crb_array }
            `,
            send_response
        )
        .end();

    }

    function send_response (err, result) {
        //! IF ERR, DISPLAY ERR MSG
        if (err) {
            winston.error('Error in creating crb');
            return next(err);
        }
        //! DISPLAY ERR MSG IF NO CRB GENERATED
        if (!result.affectedRows) return res.error('NO_RECORD_CREATED', 'No record was created');

        res.data({
            message: 'Successfully created sales',
            policy_no:       data.policy_no
        })
        .send();

    }

    start();

};

exports.verify_unique = (req, res, next) => {

    function start () {
        //! Cerify Policy number if unique
        mysql.use('master')
        .query(
            `
                SELECT  policy_no
                FROM    sales
                WHERE   policy_no = '${ req.params.policy_no }'
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {
        //! If Error, diplay error messag
        if (err) {
            winston.error('Error in verifying policy number');
            return next(err);
        }
        //! Catch for no results
        if (result.length) return res.error('INVALID_POLICY', 'Policy number is already exist');
        
        res.data({
            message: 'Successfully verified sales policy number',
        }).send();
        
    }

    start();

}

exports.retrieve = (req, res, next) => {
    //! Pagination
    let page                = parseInt(req.query.page) || 0,
        per_page            = parseInt(req.query.per_page) || 10,
        start_from          = (page - 1) < 0 ? 0 : (page -1)  * per_page,
        count;

    //! Filters
    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        policy_role         = req.query.policy_role,
        filter_policy       = req.query.filter_policy,
        filter_date         = req.query.filter_date,
        payment_status      = req.query.payment_status,
        is_cancel            = req.query.is_cancel;

    let query = ``, //! Query container
        pagination = {}; //! For Pagination

    function start () {
        //! Search 
        if (filter_policy) {
            query += ` AND (policy_no LIKE '%${ filter_policy }%'`;
            query += ` OR agent_sales_person LIKE '%${ filter_policy }%'`;
            query += ` OR reference_quote_no LIKE '%${ filter_policy }%'`;
            query += ` OR assured_name LIKE '%${ filter_policy }%'`;
            query += ` OR contact_no LIKE '%${ filter_policy }%'`;
            query += ` OR insurance_name LIKE '%${ filter_policy }%'`;
            query += ` OR classification LIKE '%${ filter_policy }%'`;
            query += ` OR policy_role LIKE '%${ filter_policy }%')`;
        }
        //! Filter for POlicy Status
        if (is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;
        //! Filter for Date
        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        //! FI=ilter for Policy Role
        if (policy_role) query += ` AND policy_role = '${ policy_role }'`;
        //! Filter for Payment Status - Overpayments
        if (payment_status == 'overpayments') query += ` AND temporary_balance < 0`;
        //! Filter for Payment Status - Underpayments
        if (payment_status == 'underpayments') query += ` AND temporary_balance > 0`;

        data_count ();

    }

    function data_count () {
        //! Count all the data
        mysql.use('master')
        .query(
            `
                SELECT COUNT(*) AS counts
                FROM    sales
                WHERE   1
                ${ query }
            `,
            retrieve_data
        ).end();

    }

    function retrieve_data (err, result, args, last_query) {
        //! If Error, diplay error message
        if (err) {
            winston.error('Error in selecting sales', last_query);
            return next(err);
        }
        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'Sales not found');
        
        //! Store all results
        count = result[0].counts;
        
        //! Select all the data
        mysql.use('master')
        .query(
            `
                SELECT  sales_id, policy_no, assured_name,
                        car_make_model, email_address, date_created,
                        IF(last_payment_date IS NULL, 'pending', 'paid') AS status
                FROM    sales
                WHERE   1
                ${ query }
                ORDER BY sales_id DESC
                LIMIT   ${ start_from }, ${ per_page }
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {
        //! If Error, diplay error message
        if (err) {
            winston.error('Error in selecting sales');
            return next(err);
        }
        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');
        //! For pagination
        pagination.TotalData = count;
        pagination.TotalPage = Math.ceil(count / per_page);

        res.data({
            result:result, 
            pagination:pagination
        })
        .send();

    }

    start ();

}

exports.export_data = (req, res, next) => {

    //! Filters
    let filter_date_from    = req.query.filter_date_from || '1190-01-01',
        filter_date_to      = req.query.filter_date_to || '2993-12-31',
        policy_role         = req.query.policy_role,
        filter_policy       = req.query.filter_policy,
        filter_date         = req.query.filter_date,
        payment_status      = req.query.payment_status,
        is_cancel            = req.query.is_cancel;

    let query = ``, //! Storage for queries
        sales_data; //! Sales items storage

    function start () {

        //! Search 
        if (filter_policy) {
            query += ` AND (policy_no LIKE '%${ filter_policy }%'`;
            query += ` OR agent_sales_person LIKE '%${ filter_policy }%'`;
            query += ` OR reference_quote_no LIKE '%${ filter_policy }%'`;
            query += ` OR assured_name LIKE '%${ filter_policy }%'`;
            query += ` OR contact_no LIKE '%${ filter_policy }%'`;
            query += ` OR insurance_name LIKE '%${ filter_policy }%'`;
            query += ` OR classification LIKE '%${ filter_policy }%'`;
            query += ` OR policy_role LIKE '%${ filter_policy }%')`;
        }
        //! Filter for POlicy Status
        if (is_cancel) query += ` AND is_cancel = '${ is_cancel }'`;
        //! Filter for Date
        if (filter_date) query += ` AND DATE(${ filter_date }) BETWEEN '${ filter_date_from }' AND '${ filter_date_to }'`;
        //! Filter for Policy Role
        if (policy_role) query += ` AND policy_role = '${ policy_role }'`;
        //! Filter for Payment Status - Overpayments
        if (payment_status == 'overpayments') query += ` AND temporary_balance < 0`;
        //! Filter for Payment Status - Underpayments
        if (payment_status == 'underpayments') query += ` AND temporary_balance > 0`;

        data ();

    }

    function data () {
        //! Retrieve Sales items
        mysql.use('master')
        .query(
            `
                SELECT  *,
                        CONCAT(classification,\'-\',insurance_quoted) as cv_pc
                FROM    sales
                WHERE   1
                ${ query }
                ORDER BY sales_id DESC
            `,
            to_export
        ).end();

    }

    function to_export (err, result, args, last_query) {
        //! If Error, diplay error message
        if (err) {
            winston.error('Error in retrieving sales', last_query);
            return next(err);
        }
        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');
        //! Store retrieved data
        sales_data = result;
        //! Put the stored data in req.body
        req.body.sales_data = sales_data;
        //! Redirect to sales export library
        data_export.sales_export(req, res, next);
    }

    start ();

}

exports.manage_cash_receipts = (req, res, next) => {

    let sales_data,
        remaining_balance,
        payment;

    function start () {

        mysql.use('master')
        .query(
            `
                SELECT  sales_id, reference_quote_no,
                        assured_name, car_make_model,
                        email_address, policy_no,
                        insurance_quoted, total_premium,
                        issuance_date, promotions, terms_of_payment,
                        automated_partial_payment, actual_balance
                FROM    sales
                WHERE   sales_id = ${ req.params.sales_id }
            `,
            retrieve_crb
        ).end();

    }

    function retrieve_crb (err, result, args, last_query) {

        //! If Error, diplay error message
        if (err) {
            winston.error('Error in retrieving sales policy', last_query);
            return next(err);
        }

        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');

        //! Store sales data
        sales_data = result;

        mysql.use('master')
        .query(
            `
                SELECT  period_term_of_payment, 
                        mode_of_payment, payee,
                        date_received, amount_to_pay,
                        amount_received,
                        IF(date_received IS NULL, 'pending', 'paid') AS status
                FROM    crb_data
                WHERE   sales_id = ${ req.params.sales_id }
                ORDER BY period_term_of_payment
            `,
            send_response
        ).end();

    }

    function send_response (err, result, args, last_query) {

         //! If Error, diplay error message
         if (err) {
            winston.error('Error in retrieving cash receipts', last_query);
            return next(err);
        }

        //! Catch for no results
        if (!result.length) return res.error('ZERO_RES', 'No results found');
        
        //! Sum all null values of amount received
        // remaining_balance = _.sumBy(result, ({ amount_received }) => amount_received ? 0 : 1)

        //! Automated computation for balance for remaining months
        /* payment = _.each(result, (c) => {
            //! If null, divide the actual balance by total count of null value and display as payment prop
            if (c.amount_received == null) c.payment = sales_data.actual_balance/remaining_balance;
            //! Else, data of payment prop will be the same as amount received 
            else c.payment = c.amount_received; 

        }); */

        //! Add all modified crb data into sales data object
        sales_data[0].crb_data = result;

        res.data({ result: sales_data })
        .send();

    }

    start ();

}

exports.soa = (req, res, next) => {

    function start () {

        mysql.use('master')
        .query(
            `
                SELECT  CURDATE() + INTERVAL 8 HOUR AS date, assured_name, delivery_address, contact_person,
                        policy_no, gross_premium, docstamps, evat, lgt,  
                        total_premium, effectivity_date
                FROM    sales
                WHERE   policy_no = '${ req.params.policy_no }'
            `,
            send_response
        ).end();

    }

    function send_response (err, result) {

         //! If Error, diplay error message
         if (err) {
            winston.error('Error in retrieving cash receipts');
            return next(err);
        }

        if (!result.length) return res.error('ZERO_RES', 'Statement of account not found');

        res.data({ result: result })
        .send();

    }

    start ();

}