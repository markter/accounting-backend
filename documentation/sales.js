/**
 * @api {get} /sales/verify/:policy_no Policy Number Verification
 * @apiGroup Sales
 * @apiVersion 1.0.0
 * 
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully verified sales policy number"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INVALID_POLICY",
 *       "message": "Policy number is already in use",
 *       "context": "Policy number is already exist"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {post} /sales Convert to Sales
 * @apiGroup Sales
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   policy_no                   Policy number
 * @apiParam {String}   agent_sales_person          Name of Agent sales person
 * @apiParam {String}   reference_quote_no          Reference number from EQ
 * @apiParam {String}   car_make_model              Insured vehicle
 * @apiParam {String}   assured_name                Client's name
 * @apiParam {String}   contact_no                  Client's contact number
 * @apiParam {String}   contact_no2                 Client's secondary contact number 
 * @apiParam {String}   email_address               Client's email address
 * @apiParam {String}   promotions                  Promotion included
 * @apiParam {String}   insurance_quoted            Insurance code
 * @apiParam {String}   insurance_name              Insurance name
 * @apiParam {String}   classification              Classification of the vehicle
 * @apiParam {Number}   fmv                         Fair Market Value
 * @apiParam {Number}   od_premium                  Own Damage premium
 * @apiParam {Number}   aon_premium                 Acts of Nature premium
 * @apiParam {Number}   bi_pd_premium               Bodily Injury and Property Damage premium
 * @apiParam {Number}   pa_premium                  Personal Accident premium
 * @apiParam {Number}   docstamps                   Docstamps fee
 * @apiParam {Number}   lgt                         Local Government Tax fee
 * @apiParam {Number}   evat                        Expanded VAlue Added Tax fee
 * @apiParam {Number}   gross_premium               Gross premium
 * @apiParam {Number}   total_premium               Total premium
 * @apiParam {Number}   terms_of_payment            Terms of payment
 * @apiParam {String}   payment_memo                Payment memo
 * @apiParam {String}   salespolicy_notes           Sales Policy notes
 * @apiParam {String}   issuance_date               Issuance Date
 * @apiParam {String}   effectivity_date            Effectivity Date
 * @apiParam {Number}   od_theft_comm               Own Damage and Theft Commision
 * @apiParam {Number}   aon_comm                    Acts of Nature Commission
 * @apiParam {Number}   bi_pd_comm                  Bodily Injury and Property Damage Commission
 * @apiParam {Number}   total_comm_compute          Automated Total Commision
 * @apiParam {Number}   automated_partial_payment   Computed Partial Payment
 * @apiParam {Number}   agency_user                 Agent ID
 * @apiParam {String}   policy_role                 Policy role
 * @apiParam {Number}   temporary_balance           Temporary Balance
 * @apiParam {Number}   actual_balance              Actual Balance
 * @apiParam {String}   mortgagee                   Mortgagee
 * @apiParam {String}   is_mortgage                 Is Mortgagee?
 * @apiParam {String}   mode_of_delivery            Mode of Delivery
 * @apiParam {String}   address                     Client's address
 * @apiParam {String}   delivery_address            Client's preferred delivery address
 * @apiParam {Number}   lto_interconnectivity       LTO Interconnectivity
 * @apiParam {String}   issued_on                   Location where Insurance Issued
 * @apiParam {String}   category                    Quote Category
 * @apiParam {String}   template                    Quote Template
 * @apiParam {String}   actual_fully_paid           Is Actual Fully Paid?
 * @apiParam {String}   fully_paid                  Fully Paid?
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "policy_no":"TEST0728",
 *     "agent_sales_person":"John Doe",
 *     "reference_quote_no":"55261-EQTPL",
 *     "car_make_model":"2021 BMW 118i M Sport 1600cc Gas A/T RWD Hatchback 5-Seater",
 *     "assured_name":"TEST",
 *     "contact_no":"123123123123",
 *     "contact_no2":"00",
 *     "email_address":"johndoe@email.com",
 *     "promotions":"None",
 *     "insurance_quoted":"MC",
 *     "insurance_name":"FPG Insurance",
 *     "classification":"PC",
 *     "fmv":2690000,
 *     "od_premium":30391.935000000005,
 *     "aon_premium":6725,
 *     "bi_pd_premium":1665,
 *     "pa_premium":0,
 *     "docstamps":4847.741875000001,
 *     "lgt":77.56387000000001,
 *     "evat":4653.832200000001,
 *     "gross_premium":38781.935000000005,
 *     "total_premium":48361.07294500001,
 *     "terms_of_payment":2,
 *     "payment_memo":"",
 *     "salespolicy_notes":"Free 24hrs roadside assistance, Fix deductible P2,000, Angel's Pizza",
 *     "issuance_date":"2021-07-21",
 *     "effectivity_date":"2021-05-05",
 *     "od_theft_comm":-7268.064999999995,
 *     "aon_comm":1681.25,
 *     "bi_pd_comm":499.5,
 *     "total_comm_compute":-5087.314999999995,
 *     "automated_partial_payment":24180.536472500004,
 *     "agency_user":21,
 *     "policy_role":"ichoose",
 *     "temporary_balance":48361.07294500001,
 *     "actual_balance":48361.07294500001,
 *     "mortgagee":null,
 *     "is_mortgage":"None",
 *     "mode_of_delivery":"Hub Pick Up",
 *     "address":"TEST",
 *     "delivery_address":"TEST",
 *     "lto_interconnectivity":0,
 *     "issued_on":"Head Office",
 *     "category":"ECTPL Website",
 *     "template":"new",
 *     "actual_fully_paid":"no",
 *     "fully_paid":"no"
 * }
 * 
 * @apiSuccess {String}   policy_no     Policy number
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully created sales",
 *     "policy_no": "TEST0728"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INC_DATA",
 *       "message": "Incomplete request data",
 *       "context": "reference_quote_no is missing"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {get} /sales Retrieve Sales
 * @apiGroup Sales
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [perPage=10]            Data per page
 * @apiParam {String}   [filter_date]           Filter date by issuance_date, date_created, effectivity_date
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [is_cancel]             Is Policy Cancelled? (yes, no)
 * @apiParam {String}   [payment_status]        Payment Status (overpayments, underpayments)
 * 
 * @apiParamExample Sample-Params:
 * 
 * page=1&per_page=10&filter_date=issuance_date&is_cancel=no&payment_status=underpayments
 * 
 * @apiSuccess {String}   sales_id          Sales Id
 * @apiSuccess {String}   policy_no         Policy number
 * @apiSuccess {String}   assured_name      Client's name
 * @apiSuccess {String}   car_make_model    Vehicle
 * @apiSuccess {String}   email_address     Client's email address
 * @apiSuccess {String}   date_created      Date created
 * @apiSuccess {String}   status            Initial payment status
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "sales_id": 40190,
 *         "policy_no": "TEST0731",
 *         "assured_name": "John Doe",
 *         "car_make_model": "2021 BMW 118i M Sport 1600cc Gas A/T RWD Hatchback 5-Seater",
 *         "email_address": "johndoe@email.com",
 *         "date_created": "2021-07-21T06:31:05.000Z",
 *         "status": "pending"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "No results found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {get} /export/sales Export Sales
 * @apiGroup Sales
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   [filter_date]           Filter date by issuance_date, date_created, effectivity_date
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [is_cancel]             Is Policy Cancelled? (yes, no)
 * @apiParam {String}   [payment_status]        Payment Status (overpayments, underpayments)
 * 
 * @apiParamExample Sample-Params:
 * 
 *filter_date=issuance_date&is_cancel=no&payment_status=underpayments
 * 
 *
 */


 /**
 * @api {get} /sales/manage/:sales_id   Retrieve Cash Receipts By Sales Id
 * @apiGroup Sales
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {Number}   sales_id                          Sales ID
 * @apiSuccess {String}   reference_quote_no                Reference Quote No. from EQ
 * @apiSuccess {String}   assured_name                      Client's name
 * @apiSuccess {String}   email_address                     Client's email address
 * @apiSuccess {String}   policy_no                         Policy number
 * @apiSuccess {String}   insurance_quoted                  Insurance Company Code
 * @apiSuccess {Number}   total_premium                     Total premium
 * @apiSuccess {String}   issuance_date                     Issuance date
 * @apiSuccess {String}   promotions                        Freebies
 * @apiSuccess {Number}   terms_of_payment                  Terms of payment
 * @apiSuccess {Number}   automated_partial_payment         Monthly payment
 * @apiSuccess {Number}   actual_balance                    Remaining balance
 * @apiSuccess {Object[]}   crb_data                        Term of payments info
 * @apiSuccess {Number}   crb_data.period_term_of_payment   Period of term of payment
 * @apiSuccess {String}   crb_data.mode_of_payment          Mode of payment
 * @apiSuccess {String}   crb_data.payee                    Payee
 * @apiSuccess {String}   crb_data.date_received            Date received
 * @apiSuccess {Number}   crb_data.amount_received          Amount received
 * @apiSuccess {Number}   crb_data.amount_to_pay            Amount to be paid
 * @apiSuccess {Number}   crb_data.status                   Payment  status
 * 
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *  "data": {
 *    "result": [
 *      {
 *        "sales_id": 40195,
 *        "reference_quote_no": "20210817-EQ",
 *        "assured_name": "John Doe",
 *        "car_make_model": "2019 TOYOTA VIOS XE CVT",
 *        "email_address": "johndoe@email.com",
 *        "policy_no": "TST-PC-0830",
 *        "insurance_quoted": "MT",
 *        "total_premium": 12781.45,
 *        "issuance_date": "2021-08-16T16:00:00.000Z",
 *        "promotions": "None",
 *        "terms_of_payment": 5,
 *        "automated_partial_payment": 2556.29,
 *        "actual_balance": 12781.45,
 *        "crb_data": [
 *          {
 *            "period_term_of_payment": 1,
 *            "mode_of_payment": null,
 *            "payee": null,
 *            "date_received": null,
 *            "amount_to_pay": 2556.29,
 *            "amount_received": null,
 *            "status": "pending"
 *          },
 *          {
 *            "period_term_of_payment": 2,
 *            "mode_of_payment": null,
 *            "payee": null,
 *            "date_received": null,
 *            "amount_to_pay": 2556.29,
 *            "amount_received": null,
 *            "status": "pending"
 *          },
 *          {
 *            "period_term_of_payment": 3,
 *            "mode_of_payment": null,
 *            "payee": null,
 *            "date_received": null,
 *            "amount_to_pay": 2556.29,
 *            "amount_received": null,
 *            "status": "pending"
 *          },
 *          {
 *            "period_term_of_payment": 4,
 *            "mode_of_payment": null,
 *            "payee": null,
 *            "date_received": null,
 *            "amount_to_pay": 2556.29,
 *            "amount_received": null,
 *            "status": "pending"
 *          },
 *          {
 *            "period_term_of_payment": 5,
 *            "mode_of_payment": null,
 *            "payee": null,
 *            "date_received": null,
 *            "amount_to_pay": 2556.29,
 *            "amount_received": null,
 *            "status": "pending"
 *          }
 *        ]
 *      }
 *    ]
 *  },
 *  "success": true
 *}
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "No results found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

 /**
 * @api {get} /sales/soa/:policy_no   Retrieve Statement of Account
 * @apiGroup Sales
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {String}   date              Current date
 * @apiSuccess {String}   assured_name      Client's name
 * @apiSuccess {String}   delivery_address  Client's delivery address
 * @apiSuccess {String}   contact_person    Contact person
 * @apiSuccess {String}   policy_no         Policy number
 * @apiSuccess {Number}   gross_premium     Gross premium
 * @apiSuccess {Number}   docstamps         Docstamps
 * @apiSuccess {Number}   evat              E-Vat
 * @apiSuccess {Number}   lgt               Local government tax
 * @apiSuccess {Number}   total_premium     Total premium
 * @apiSuccess {String}   effectivity_date  Effective date
 * 
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "date": "2021-08-31T00:00:00.000Z",
 *         "assured_name": "John Doe",
 *         "delivery_address": "Metro Manila, PH",
 *         "contact_person": null,
 *         "policy_no": "TST-PC-08-312021",
 *         "gross_premium": 449.08,
 *         "docstamps": 56.135,
 *         "evat": 53.8896,
 *         "lgt": 0.89816,
 *         "total_premium": 625.0027599999999,
 *         "effectivity_date": "2021-09-30T16:00:00.000Z"
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Statement of account not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */