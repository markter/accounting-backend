/**
 * @api {post} /commission Computation for Mercantile/Standard Insurance
 * @apiGroup Commission
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   insurance_name              Insurance name
 * @apiParam {String}   classification              Classification of the vehicle (PC, CV, Truck, Motorcycle)
 * @apiParam {String}   category                    Insurance Category (Motor/Car, ECTPL, ECTPL Website)
 * @apiParam {Number}   fmv                         Fair Market Value
 * @apiParam {Number}   od_premium                  Own Damage premium
 * @apiParam {Number}   aon_premium                 Acts of Nature premium
 * @apiParam {Number}   bi_pd_premium               Bodily Injury and Property Damage premium
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "insurance_name": "Mercantile Insurance",
 *     "classification": "PC",
 *     "category": "Motor/Car",
 *     "od_premium": 5976.93,
 *     "aon_premium": 1125,
 *     "bi_pd_premium": 1665,
 *     "fmv": 450000
 * }
 * 
 * @apiSuccess {Number}   od_theft_comm     Own Damage and Theft Commission
 * @apiSuccess {Number}   aon_comm          Acts of Nature Commission
 * @apiSuccess {Number}   bi_pd_comm        Bodily Injury and Property Damage Commission
 * @apiSuccess {Number}   total_commission  Total Commission
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "od_theft_comm": 2402.881710000001,
 *     "aon_comm": 225,
 *     "bi_pd_comm": 416.25,
 *     "total_commission": 3044.131710000001
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INC_DATA",
 *       "message": "Incomplete request data",
 *       "context": "insurance_name is missing"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {post} /commission Computation for FPG Insurance
 * @apiGroup Commission
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   insurance_name              Insurance name
 * @apiParam {String}   category                    Insurance Category (Motor/Car, ECTPL, ECTPL Website)
 * @apiParam {String}   classification              Classification of vehicle (PC, CV, Truck, Motorcycle)
 * @apiParam {Number}   gross_premium               Gross premium

 * 
 * @apiParamExample Sample-Params:
 * {
 *     "insurance_name": "FPG Insurance",
 *     "category": "Motor/Car",
 *     "gross_premium": 11325
 * }
 * 
 * @apiSuccess {Number}   total_commission  Total Commission
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "total_commission": 2831.25
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INC_DATA",
 *       "message": "Incomplete request data",
 *       "context": "category is missing"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {post} /commission Computation for Mercantile Insurance - ECTPL
 * @apiGroup Commission
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   insurance_name              Insurance name
 * @apiParam {String}   classification              Classification of the vehicle (PC, CV, Truck, Motorcycle)
 * @apiParam {String}   category                    Insurance Category (Motor/Car, ECTPL, ECTPL Website)
 * @apiParam {Number}   total_premium               Total Premium
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "insurance_name": "Mercantile Insurance",
 *     "classification": "Motorcycle",
 *     "category": "ECTPL",
 *     "total_premium": 315
 * }
 * 
 * @apiSuccess {Number}   total_commission  Total Commission
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "total_commission": 135
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INC_DATA",
 *       "message": "Incomplete request data",
 *       "context": "insurance_name is missing"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {post} /commission Computation for Standard Insurance - ECTPL
 * @apiGroup Commission
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   insurance_name              Insurance name
 * @apiParam {String}   classification              Classification of the vehicle (PC, CV, Truck, Motorcycle)
 * @apiParam {String}   category                    Insurance Category (Motor/Car, ECTPL, ECTPL Website)
 * @apiParam {Number}   total_premium               Total Premium
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "insurance_name": "Mercantile Insurance",
 *     "classification": "Motorcycle",
 *     "category": "ECTPL Website",
 *     "total_premium": 315
 * }
 * 
 * @apiSuccess {Number}   total_commission  Total Commission
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "total_commission": 110.25
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INC_DATA",
 *       "message": "Incomplete request data",
 *       "context": "classification is missing"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */