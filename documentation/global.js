/**
 * @api {get} /category Category List
 * @apiGroup Global Tags
 * @apiVersion 1.0.0
 * 
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "items": [
 *       {
 *         "id": 1,
 *         "category": "Motor/Car"
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *   "errors": [
 *       {
 *           "code": "ZERO_RES",
 *           "message": "Database no result",
 *           "context": "No results found"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */