/**
 * @api {get} /bank_filing Retrieve Bank Filings
 * @apiGroup Bank Filing
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [effectivity_date]      Effectivity date
 * 
 * @apiParamExample Sample-Params:
 * page=1&per_page=10&search=&effectvity_date
 * 
 * @apiSuccess {Number}   sales_id              Sales Id
 * @apiSuccess {String}   assured_name          Client's name
 * @apiSuccess {String}   policy_no             Policy number
 * @apiSuccess {String}   date_received         Date received
 * @apiSuccess {String}   reference_quote_no    Reference Quote number
 * @apiSuccess {String}   mortgagee             Mortgagee
 * @apiSuccess {String}   status                Record status (Filed, Refiled, Online)
 * @apiSuccess {Number}   is_collected          Is collected? (1, 0)
 * @apiSuccess {String}   date_filed            Date filed
 * @apiSuccess {Number}   is_filed              Is Filed (1,0)
 * @apiSuccess {String}   effectivity_date      Effectivity date
 * @apiSuccess {String}   notes                 Bank filing notes
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "sales_id": 40178,
 *         "assured_name": "John Doe",
 *         "policy_no": "TST-PC-1",
 *         "date_received": "2021-07-13T16:00:00.000Z",
 *         "reference_quote_no": "1-TSTRRNO",
 *         "mortgagee": "null",
 *         "status": "Filed",
 *         "rider": "Alpha",
 *         "date_filed": "2021-07-13T16:00:00.000Z",
 *         "is_filed": 0,
 *         "effectivity_date": "2021-07-31T16:00:00.000Z",
 *         "notes": "This is a sample Bank Filing note"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Records for bank filing not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */