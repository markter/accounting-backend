/**
 * @api {post} /auth/uname_email Verify Username or Email
 * @apiGroup Authentication
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   uname_email     Username or Email of registered user
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "uname_email": "johndoe"
 * }
 * 
 * @apiSuccess {String}   id            User ID
 * @apiSuccess {String}   username      Registered username
 * @apiSuccess {String}   email         Registered email
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "items": [
 *       {
 *         "id": "381764c6-5321-4a47-8fcf-98b4f96c904d",
 *         "username": "johndoe",
 *         "email": "johndoe@email.com"
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *       {
 *           "code": "LOG_FAIL",
 *           "message": "Log-In failed,
 *           "context": "Invalid username or email"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */


/**
 * @api {post} /auth/login  Login
 * @apiGroup Authentication
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   uname_email     Username or Email of registered user
 * @apiParam {String}   password        Password of registered user
 * 
 * @apiParamExample Sample-Params:
 * {
 *      "uname_email": "johndoe",
 *      "password": "test123"
 * }
 * 
 * @apiSuccess {String}   id                    User ID
 * @apiSuccess {String}   username              Registered username
 * @apiSuccess {String}   email                 Registered email
 * @apiSuccess {String}   agency_role           User role
 * @apiSuccess {String}   module_restriction    User Module Restriction List
 * @apiSuccess {String}   permission_task       User Permission
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "items": [
 *       {
 *          "id": "381764c6-5321-4a47-8fcf-98b4f96c904d",
 *          "username": "johndoe",
 *          "email": "johndoe@email.com",
 *          "agency_role": "superadmin",
 *          "module_restriction": "sales_policy, issuance_view, crb, deposits, bank_balances_view, remittance_record_view, remittance_insurance_view, overdue_monthly, collection_reports, backup_record, users",
 *          "permission_task": "execute",
 *          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiM2JhMWZlNTkzMGZiZGIxZDRkOGU4Mzg0NGNlZGUyY2E6MmZlYWNlNzIxZTVkYTc2YzU5YTIyZTU1MDg2M2I2NDBiMzIzOTRkOWViMjcwYmNmOTdiOWM1OTI1MGU0ZDJjN2NkMmI4M2JiYTg0NGRjYzJhMjkyOTVkMzVhYTBkYmMyY2JkODNmNjVlYzY5YjVmYzU0MzM4Y2YwMmU5YzljMmVkNjhhN2UyYWMxMmM4MDBlZjUyZDgwOWJmODhlMTY1NzczNmY5ZmY0MzdkODIwZDU5Nzc5NTAwZGJhNzBkOTllYjU4MWE0ODhmMDE0Mzc5YjUxYmEyOTAwZjY3Y2Q0MzhkZDE5YTk4YWYxYTk1MWFhZTBiYzY3ODBmOTYzZWMxYTU0ZDQwNDZkMDVmNTVkMzEyZmY3MmNjZTEzZmVlYWViZWU0NjJiMjgzMGViMTcyODA4MDI1ODBlOGU1MWNmNGQxZDA5NmZiZmVkODc1MWJhNzViOGEyMzQ3Nzk5YWRkMmIzOWIyYjIyMTUxOThmOWFiYzM2NmVmN2E5MGZiMGI1NDc0ZmFiM2JiMGZhMjMzNDAwNDExMjBkOTM5ODdlNjBjNWJiZWU3ZmNjMmVkNTkzYTNhYzFlZjU5NjJlZWVhOTI0YjgzMWMyMDViOGRhMDlkNzBiYmI3ZTA0NDNkZDU4ZDgzNzM4NDZlNWI3MDNjZmQxMGI4Mjg3MGY5ZmJiNjVlNjg1MWIwNDRjZGJkNzA0MTUwYjcyYmY4NDk0MDBkOWZlZjQ0NzgzYTFiNWExMDkyYTFlYmQ3MTZlZDQ3YzM3MGE2MDM3OTQ0OGMyOTM1ZGNkOWJmZmI2NGE0Mzg1OTA5MGVkMjQyYWZlYzQ1NGQxZGRjNjM4NTI0Yzc2N2YwZTI2YWI5MTZkYWQ3M2I3ZDk2NTJkNjJhNTk3NjkiLCJpYXQiOjE2MjYzNDYwODgsImV4cCI6MTY0NDQ5MDA4OH0.OnXcxh0z-Yo3iELPUgCDW-KldkAgmyRRAQn-O8oniq0"
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *       {
 *           "code": "LOG_FAIL",
 *           "message": "Log-In failed,
 *           "context": "Incorrect Password"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {post} /auth/logout  Logout
 * @apiGroup Authentication
 * @apiVersion 1.0.0
 * 
 * @apiHeader {String} x-access-token Token from login
 * 
 * 
 * @apiSuccess {String}   id            User ID
 * @apiSuccess {String}   username      Registered username
 * @apiSuccess {String}   email         Registered email
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "items": [
 *       {
 *         "message": "User successfully logged out"
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 403 Forbidden
 * {
 *   "errors": [
 *     {
 *       "code": "UNAUTH",
 *       "message": "Unauthorized access",
 *       "context": "Failed to authenticate token."
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */