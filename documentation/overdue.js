/**
 * @api {get} /overdue_monthly Overdue Monthly 
 * @apiGroup Overdue Monthly
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date by issuance_date, date_created, effectivity_date
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [overdue_date]          Filter date for overdue accounts
 * @apiParam {String}   [next_follow_up_date]   Filter date for follow up date
 * 
 * @apiParamExample Sample-Params:
 * 
 * page=1&per_page=10&search=test&filter_date=date_created&overdue_date=2021-08-25&next_follow_up_date=2021-08-25
 * 
 * @apiSuccess {Number}   sales_id                  Sales Id
 * @apiSuccess {String}   policy_no                 Policy number
 * @apiSuccess {String}   assured_name              Client's name
 * @apiSuccess {String}   email_address             Client's email address
 * @apiSuccess {String}   contact_no                Client's contact number
 * @apiSuccess {String}   contact_no2               Client's other contact number
 * @apiSuccess {String}   agent_sales_person        Agent sales person
 * @apiSuccess {String}   insurance_name            Insurance company
 * @apiSuccess {String}   classification            Vehicle classification
 * @apiSuccess {Number}   gross_premium             Gross premium
 * @apiSuccess {Number}   total_premium             Total premium
 * @apiSuccess {Number}   terms_of_payment          Terms of payment
 * @apiSuccess {String}   issuance_date             Date issued
 * @apiSuccess {String}   effectivity_date          Effective date
 * @apiSuccess {String}   date_created              Date created
 * @apiSuccess {Number}   total_comm_compute        Automated computation of total commission
 * @apiSuccess {Number}   total_comm_manual         Manual computation of total commission
 * @apiSuccess {String}   last_payment_date         Last payment date
 * @apiSuccess {String}   next_follow_up_date       Follow up date
 * @apiSuccess {String}   actual_sum_total_amount   Actual sum total amount
 * @apiSuccess {String}   actual_balance            Actual balance
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *          "sales_id": 1,
 *          "policy_no": "TST-PC-0825",
 *          "assured_name": "John Doe",
 *          "email_address": "johndoe@email.com",
 *          "contact_no": "09123456789",
 *          "contact_no2": "09987654321",
 *          "agent_sales_person": "Jane",
 *          "insurance_name": "COCOGEN",
 *          "classification": "PC",
 *          "gross_premium": 14550.25,
 *          "total_premium": 18144.16175,
 *          "terms_of_payment": 1,
 *          "issuance_date": "2021-08-01T16:00:00.000Z",
 *          "effectivity_date": "2021-07-26T16:00:00.000Z",
 *          "date_created": "2021-08-05T07:19:44.000Z",
 *          "total_comm_compute": 3637.5625,
 *          "total_comm_manual": 7458.33,
 *          "last_payment_date": "2021-08-03T16:00:00.000Z",
 *          "next_follow_up_date": "2021-08-24T16:00:00.000Z",
 *          "actual_sum_total_amount": 6048.16,
 *          "actual_balance": 12096.00175
 *      }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "No overdue accounts found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */