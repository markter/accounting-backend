/**
 * @api {get} /crb Retrieve Cash Receipts Book 
 * @apiGroup Accounting
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date (date_use_for_filter, date_received, cheque_date, bd_date_deposited)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [mode_of_payment]       Remittance type (cash, cheque, bank_deposit, paypal, discount, commission, 2307_tax, discount_as_couriers_fee, free_ctpl)
 * @apiParam {String}   [bd_bank_account]       Bank account
 * @apiParam {String}   [payee]                 Payable to
 * 
 * @apiParamExample Sample-Params:
 * 
 * page=1&per_page=10&filter_date=date_use_for_filter
 * 
 * @apiSuccess {Number}   crb_id                    Cash receipts book ID
 * @apiSuccess {String}   date_use_for_filter       Date Use for filter
 * @apiSuccess {String}   mode_of_payment           Mode of payment
 * @apiSuccess {Number}   amount_received           AMount received
 * @apiSuccess {String}   assured_name              Client's name
 * @apiSuccess {String}   policy_no                 Sales policy number
 * @apiSuccess {Number}   period_term_of_payment    Terms of payment period
 * @apiSuccess {String}   is_deposited              Is deposited? (yes, no)
 * @apiSuccess {String}   cheque_issuing_bank       Cheque issuing bank
 * @apiSuccess {String}   cheque_branch             Cheque branch
 * @apiSuccess {String}   cheque_number             Cheque number
 * @apiSuccess {String}   cheque_date               Cheque date
 * @apiSuccess {String}   bd_date_deposited         Date deposited
 * @apiSuccess {String}   bd_bank_account           Bank account
 * @apiSuccess {String}   bd_reference_number       Reference number
 * @apiSuccess {Object[]} collections               Collections info
 * @apiSuccess {Number}   collections.cash          Total Cash
 * @apiSuccess {Number}   collections.cheque        Total Cheque
 * @apiSuccess {Number}   collections.bank_deposit  Total bank deposit
 * @apiSuccess {Number}   collections.paypal        Total Paypal
 * @apiSuccess {Number}   collections.total_amount  Total amount
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "crb_id": 282835,
 *         "date_use_for_filter": "2021-07-09T16:00:00.000Z",
 *         "mode_of_payment": "cheque",
 *         "amount_received": 20652,
 *         "assured_name": "John Doe",
 *         "policy_no": "SI-PCV-333576714",
 *         "period_term_of_payment": 1,
 *         "is_deposited": "no",
 *         "cheque_issuing_bank": "BDO",
 *         "cheque_branch": "Las Pinas",
 *         "cheque_number": "1816735",
 *         "cheque_date": "2021-07-09T16:00:00.000Z",
 *         "bd_date_deposited": null,
 *         "bd_bank_account": null,
 *         "bd_reference_number": null
 *       }
 *     ],
 *     "collections": {
 *       "cash": 58333.66,
 *       "cheque": 1819378.3199999996,
 *       "bank_deposit": 272637.76,
 *       "total_amount": 2174653.269999999,
 *       "paypal": 24303.530000000002
 *     },
 *     "pagination": {
 *       "TotalData": 432,
 *       "TotalPage": 44
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Cash Receipts Book records not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {get} /deposits/manage/:crb_id Retrieve Manage Deposit 
 * @apiGroup Accounting
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   crb_id  Cash receipts book ID
 * 
 * @apiSuccess {String}   policy_no                     Sales policy number
 * @apiSuccess {String}   insurance_quoted              Insurance company code
 * @apiSuccess {String}   assured_name                  Client's name
 * @apiSuccess {Number}   total_premium                 Total premium
 * @apiSuccess {Number}   terms_of_payment              Terms of payment
 * @apiSuccess {String}   reference_quote_no            EQ reference number
 * @apiSuccess {String}   classification                Classification
 * @apiSuccess {Number}   automated_partial_payment     Automated partial payment
 * @apiSuccess {Number}   period_term_of_payment        Terms of payment period
 * @apiSuccess {String}   date_received                 Date received
 * @apiSuccess {String}   cheque_issuing_bank           Cheque issuing bank
 * @apiSuccess {String}   cheque_date                   Cheque date
 * @apiSuccess {String}   mode_of_payment               Mode of payment
 * @apiSuccess {Number}   amount_received               Amount received
 * @apiSuccess {String}   cheque_branch                 Cheque branch
 * @apiSuccess {String}   cheque_number                 Cheque number
 * @apiSuccess {String}   bd_date_deposited             Date deposited
 * @apiSuccess {String}   bd_bank_account               Bank account
 * @apiSuccess {String}   bd_reference_number           Reference number
 * @apiSuccess {Number}   actual_balance                Actual balance
 * @apiSuccess {Number}   actual_sum_total_amount       Actual sum total amount
 * @apiSuccess {Number}   temporary_balance             Temporary balance
 * @apiSuccess {Number}   temporary_sum_total_amount    Temporary sum total amount
 * @apiSuccess {Number}   total_cancel_receipts         Total cancel receipts
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "items": [
 *       {
 *         "policy_no": "SI-PCV-500576714",
 *         "insurance_quoted": "SI",
 *         "assured_name": "John Doe",
 *         "total_premium": 20651.49218,
 *         "terms_of_payment": 1,
 *         "reference_quote_no": "615398-EQ",
 *         "classification": "CV",
 *         "automated_partial_payment": 20651.49218,
 *         "period_term_of_payment": 1,
 *         "date_received": "2021-07-11T16:00:00.000Z",
 *         "cheque_issuing_bank": "BDO",
 *         "cheque_date": "2021-07-09T16:00:00.000Z",
 *         "mode_of_payment": "cheque",
 *         "amount_received": 20652,
 *         "cheque_branch": "Navotas",
 *         "cheque_number": "1816735",
 *         "bd_date_deposited": null,
 *         "bd_bank_account": null,
 *         "bd_reference_number": null,
 *         "actual_balance": 20651.49218,
 *         "actual_sum_total_amount": 0,
 *         "temporary_balance": -0.507819999998901,
 *         "temporary_sum_total_amount": 20652,
 *         "total_cancel_receipts": 0
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Cash Receipts Book record not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {get} /deposits/summary Retrieve Deposit Summary 
 * @apiGroup Accounting
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date (date_use_for_filter, date_received, cheque_date, bd_date_deposited)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [mode_of_payment]       Remittance type (cash, cheque, bank_deposit, paypal, discount, commission, 2307_tax, discount_as_couriers_fee, free_ctpl)
 * @apiParam {String}   [bd_bank_account]       Bank account
 * @apiParam {String}   [payee]                 Payable to
 * 
 * @apiParamExample Sample-Params:
 * 
 * page=1&per_page=10&filter_date=date_use_for_filter
 * 
 * @apiSuccess {String}   date_use_for_filter       Date Use for filter
 * @apiSuccess {String}   mode_of_payment           Mode of payment
 * @apiSuccess {Number}   amount_received           AMount received
 * @apiSuccess {String}   assured_name              Client's name
 * @apiSuccess {String}   policy_no                 Sales policy number
 * @apiSuccess {Number}   period_term_of_payment    Terms of payment period
 * @apiSuccess {String}   is_deposited              Is deposited? (yes, no)
 * @apiSuccess {String}   cheque_issuing_bank       Cheque issuing bank
 * @apiSuccess {String}   cheque_branch             Cheque branch
 * @apiSuccess {String}   cheque_number             Cheque number
 * @apiSuccess {String}   cheque_date               Cheque date
 * @apiSuccess {String}   bd_date_deposited         Date deposited
 * @apiSuccess {String}   bd_bank_account           Bank account
 * @apiSuccess {String}   bd_reference_number       Reference number
 * @apiSuccess {Object[]} collections               Collections info
 * @apiSuccess {Number}   collections.cash          Total Cash
 * @apiSuccess {Number}   collections.cheque        Total Cheque
 * @apiSuccess {Number}   collections.bank_deposit  Total bank deposit
 * @apiSuccess {Number}   collections.paypal        Total Paypal
 * @apiSuccess {Number}   collections.total_amount  Total amount
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 *     
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "date_use_for_filter": "2021-07-12T16:00:00.000Z",
 *         "mode_of_payment": "bank_deposit",
 *         "amount_received": 626,
 *         "assured_name": "John Doe",
 *         "policy_no": "MC-TPL-CH-21-099991005-00",
 *         "period_term_of_payment": 1,
 *         "is_deposited": "yes",
 *         "cheque_issuing_bank": null,
 *         "cheque_branch": null,
 *         "cheque_number": null,
 *         "cheque_date": null,
 *         "bd_date_deposited": "2021-07-12T16:00:00.000Z",
 *         "bd_bank_account": "BPI",
 *         "bd_reference_number": "23"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 72676,
 *       "TotalPage": 7268
 *     },
 *     "collections": {
 *       "cash": 17861967.531000007,
 *       "cheque": 96152526.78999953,
 *       "bank_deposit": 240148067.97299904,
 *       "total_amount": 366626029.4869986,
 *       "paypal": 12463467.193
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Cash Receipts Book records not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {post} /deposits Create Deposit
 * @apiGroup Accounting
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   date_of_remittance      Date of remittance
 * @apiParam {String}   insurance_name          Insurance company
 * @apiParam {String}   insurance_quoted        Insurance company code
 * @apiParam {String}   policy_role             Ppolicy role
 * @apiParam {String}   remittance_ref_no       Remittance reference number
 * @apiParam {String}   remittance_type         Remittance type (net_remittance, gross_remittance)
 * @apiParam {String}   [cheque_account_name]   Cheque account name (for net_remittance only)
 * @apiParam {String}   [cheque_date]           Cheque date (for net_remittance only)
 * @apiParam {String}   [cheque_number]         Cheque number (for net_remittance only)
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "cheque_account_name": "BDO",
 *     "cheque_date": "2021-09-28",
 *     "cheque_number": "TSTREMIT-0928",
 *     "date_of_remittance": "2021-09-28",
 *     "insurance_name": "Standard Insurance",
 *     "insurance_quoted": "SI",
 *     "policy_role": "ichoose",
 *     "remittance_ref_no": "TST NET REMIT 28",
 *     "remittance_type": "net_remittance"
 * }
 *
 * 
 * @apiSuccess {String}   id                Remittance record ID
 * @apiSuccess {String}   insurance_name    Insurance company
 * @apiSuccess {String}   insurance_quoted  Insurance company code
 * @apiSuccess {String}   remittance_ref_no Remittance reference number
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully created remittance record",
 *     "id": 4954,
 *     "insurance_name": "Standard Insurance",
 *     "insurance_quoted": "SI",
 *     "remittance_ref_no": "TST NET REMIT 28"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *       {
 *           "code": "INVALID_REMITTANCE_REF_NO",
 *           "message": "Reference number is already in use",
 *           "context": "Reference number is already in use"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */