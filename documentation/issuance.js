/**
 * @api {get} /issuance View Issuance
 * @apiGroup Issuance
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]            Data per page
 * @apiParam {Number}   [converted_to_sales]    Filter converted to sales (value 0 or 1)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [search]                Data search
 *
 * 
 * @apiSuccess {Number}   issuance_id       Issuance ID
 * @apiSuccess {String}   reference_number  Reference number
 * @apiSuccess {String}   name              Client's name
 * @apiSuccess {String}   car_insurance_for Insured vehicle
 * @apiSuccess {String}   email             Client's email
 * @apiSuccess {String}   date_created      Date of Issuance created
 * @apiSuccess {String}   address           Client's address
 * @apiSuccess {String}   post_code         Postal Code
 * @apiSuccess {String}   mobile            Mobile number
 * @apiSuccess {String}   phone1            Other contact number
 * @apiSuccess {String}   insurance_company Insurance company
 * @apiSuccess {String}   mode_of_delivery  Mode of delivery
 * @apiSuccess {String}   effectivity_date  Effectivity date
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *          "issuance_id": 38865,
 *          "reference_number": "619473-EQ",
 *          "name": "John Doe",
 *          "car_insurance_for": "2020 TOYOTA HiAce Commuter Deluxe 3000cc Diesel M/T RWD Van 15-Seater",
 *          "email": "johndoe@email.com",
 *          "date_created": "2021-07-17T04:50:34.000Z",
 *          "address": "Metro Manila, PH",
 *          "post_code": "4027",
 *          "mobile": "09123456789",
 *          "phone1": "09987654321",
 *          "insurance_company": "Mercantile Insurance",
 *          "mode_of_delivery": "Hub Pick Up",
 *          "effectivity_date": "2021-07-17T16:00:00.000Z"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *       {
 *           "code": "ZERO_RES",
 *           "message": "Database no result",
 *           "context": "No results found"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {get} /issuance/:id View Issuance by Id
 * @apiGroup Issuance
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {Number} issuance_id                     Issuance Id
 * @apiSuccess {String} name                            Client's name
 * @apiSuccess {String} address                         Client's address
 * @apiSuccess {String} post_code                       Client's postal code
 * @apiSuccess {String} email                           Client's email
 * @apiSuccess {String} mobile                          Client's mobile number
 * @apiSuccess {String} phone1                          Client's other contact number
 * @apiSuccess {String} car_insurance_for               Vehicle
 * @apiSuccess {String} vin_chassi_no                   Vehicle Chassis number
 * @apiSuccess {String} motor_engine_no                 Vehicle Motor engine number
 * @apiSuccess {String} color                           Vehicle color
 * @apiSuccess {String} plate_conduction_no             Vehicle plate/conduction number
 * @apiSuccess {Number} fair_market_value               Vehicle fair market value
 * @apiSuccess {String} classification                  Vehicle Classification
 * @apiSuccess {String} mortgagee                       Mortgagee
 * @apiSuccess {Number} acts_of_god_included            Is AON/AOG included? (1, 0)
 * @apiSuccess {Number} is_mortgage                     Is mortgage? (1, 0)
 * @apiSuccess {Number} bodily_injury_property_damage   BI/PD value
 * @apiSuccess {Number} personal_accident_amount        Personal Accident value
 * @apiSuccess {Number} rate                            Insurance company rate
 * @apiSuccess {Number} acts_of_god_rate                Acts of God/Nature rate
 * @apiSuccess {Number} fair_market_value_rate          Fair market value premium
 * @apiSuccess {Number} bodily_injury_premium           Bodily Injury premium
 * @apiSuccess {Number} property_damage_premium         Property Damage premium
 * @apiSuccess {Number} bi_pd_premium                   Sum of BI/PD premium
 * @apiSuccess {Number} pa_premium                      Personal Accident premium
 * @apiSuccess {Number} aon_premium                     Acts of Nature/God premium
 * @apiSuccess {Number} od_premium                      Own Damage/Theft premium
 * @apiSuccess {Number} od_aon_premium                  Sum of Own Damage/Theft and Acts of Nature/God premium
 * @apiSuccess {Number} annual_premiums                 Annual premium
 * @apiSuccess {Number} lto_interconnectivity           LTO Interconnectivity (For TPL only)
 * @apiSuccess {Number} docstamps                       Docstamps
 * @apiSuccess {Number} evat                            Evat
 * @apiSuccess {Number} lgt                             Local Government Tax
 * @apiSuccess {Number} total_premiums                  Total premium
 * @apiSuccess {String} effectivity_date                Effectivity date
 * @apiSuccess {String} memo                            Issuance memo
 * @apiSuccess {String} user                            System User                            
 * @apiSuccess {String} agent_sales_person              Agent Sales Person
 * @apiSuccess {String} date_created                    Date converted to Issuance
 * @apiSuccess {String} reference_number                EQ reference number
 * @apiSuccess {String} delivery_date                   Delivery date
 * @apiSuccess {String} accessories_memo                Accessories memo
 * @apiSuccess {String} payment_memo                    Payment memo
 * @apiSuccess {String} category                        Insurance Category
 * @apiSuccess {String} location                        Location where Issuance issued
 * @apiSuccess {String} template                        Template (new, renewal)
 * @apiSuccess {String} promotions                      Freebies
 * @apiSuccess {String} mode_of_delivery                Mode of delivery
 * @apiSuccess {String} providerCode                    Insurance company code
 * @apiSuccess {Number} bi_pd_comm                      BI/PD commission
 * @apiSuccess {Number} od_theft_comm                   Own Damage/Theft commission
 * @apiSuccess {Number} aon_comm                        Acts of Nature/God commission
 * @apiSuccess {Number} total_commission                Total commission
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "issuance_id": 38865,
 *         "name": "John Doe",
 *         "address": "Metro Manila, PH",
 *         "post_code": "1226",
 *         "email": "johndoe@email.com",
 *         "mobile": "09123456789",
 *         "phone1": "09987654321",
 *         "car_insurance_for": "2020 TOYOTA HiAce Commuter Deluxe 3000cc Diesel M/T RWD Van 15-Seater",
 *         "vin_chassi_no": "AWSE1231",
 *         "motor_engine_no": "ASGDH123123",
 *         "color": "SILVER",
 *         "plate_conduction_no": "ASD1234",
 *         "fair_market_value": 1450000,
 *         "classification": "PC",
 *         "mortgagee": "TFSPH",
 *         "acts_of_god_included": 1,
 *         "is_mortgage": "Yes",
 *         "bodily_injury_property_damage": 200000,
 *         "personal_accident_amount": 250000,
 *         "rate": 1,
 *         "acts_of_god_rate": 0.25,
 *         "fair_market_value_rate": 15301.935,
 *         "bodily_injury_premium": 420,
 *         "property_damage_premium": 1245,
 *         "bi_pd_premium": 1665,
 *         "pa_premium": 0,
 *         "aon_premium": 3625,
 *         "od_premium": 15301.935,
 *         "od_aon_premium": 18926.934999999998,
 *         "annual_premiums": 20591.934999999998,
 *         "lto_interconnectivity": 0,
 *         "docstamps": 2573.9918749999997,
 *         "evat": 2471.03,
 *         "lgt": 41.18,
 *         "total_premiums": 25678.142945,
 *         "effectivity_date": "2021-07-17T16:00:00.000Z",
 *         "memo": "Free 24hrs roadside assistance, Fix deductible P2,000",
 *         "user": "user@gmail.com",
 *         "agent_sales_person": "Lorem Ipsum",
 *         "date_created": "2021-07-17T04:50:34.000Z",
 *         "reference_number": "619473-EQ",
 *         "delivery_date": "2021-07-16T16:00:00.000Z",
 *         "accessories_memo": "NONE",
 *         "payment_memo": "ONE TIME PAYMENT",
 *         "category": "Motor/Car",
 *         "location": "Molino Hub",
 *         "template": "new",
 *         "promotions": "FREE Dash Cam(1000)",
 *         "mode_of_delivery": "Hub Pick Up",
 *         "providerCode": "MT",
 *         "od_theft_comm": 2621.1129449999994,
 *         "aon_comm": 650,
 *         "bi_pd_comm": 416.25,
 *         "total_commission": 3949.412945000002
 *       }
 *     ]
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *       {
 *           "code": "ZERO_RES",
 *           "message": "Database no result",
 *           "context": "No results found"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {get} /export/issuance Export Issuance
 * @apiGroup Issuance
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   [filter_date]           Filter date by issuance_date, effectivity_date
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {Number}   [converted_to_sales]    Converted to sales (1, 0)
 * 
 * @apiParamExample Sample-Params:
 * 
 *converted_to_sales=1&filter_date=date_created
 *
 */