/**
 * @api {get} /initial_payments Retrieve Initial Payments
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Date filter (date_received, date_deposited)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [payment]               Type of payment (bank_deposit, paypal)
 * @apiParam {String}   [bank]                  Name of the bank (BPI, BDO, GCash, Metrobank, Paypal)
 * @apiParam {String}   [payee]                 Payable to (iChoose, iChoose Agency, FPG Insurance, etc.)
 * @apiParam {String}   [mode_of_delivery]      Mode of delivery (Courier, Courier via Rider, Hub Pick Up, ePolicy)
 * @apiParam {String}   [is_cancel]             Policy status (yes, no)
 * 
 * @apiParamExample Sample-Params:
 * page=1&per_page=10&filter_date=date_received&search=John&filter_date_from=2021-03-01&filter_date_to=2021-08-02&payment=bank_deposit&bank=BPI&payee=iChoose&mode_of_delivery=Hub Pick Up&is_cancel=no
 * 
 * @apiSuccess {Number}   sales_id              Sales Id
 * @apiSuccess {String}   assured_name          Client's name
 * @apiSuccess {Number}   amount_received       Amount received
 * @apiSuccess {String}   policy_no             Policy number
 * @apiSuccess {String}   date_received         Date received
 * @apiSuccess {String}   bd_date_deposited     Date deposited
 * @apiSuccess {String}   mode_of_payment       Type of payment
 * @apiSuccess {String}   bd_bank_account       Name of the bank
 * @apiSuccess {Number}   is_collected          Is collected? (1, 0)
 * @apiSuccess {String}   delivery_address      Delivery address
 * @apiSuccess {String}   mode_of_delivery      Mode of delivery
 * @apiSuccess {String}   agent_sales_person    Agent sales person
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *  "data": {
 *    "result": [
 *      {
 *        "sales_id": 1,
 *        "assured_name": "John Doe",
 *        "amount_received": 6000,
 *        "policy_no": "TST-POL-0802",
 *        "date_received": "2021-08-02T16:00:00.000Z",
 *        "bd_date_deposited": "2021-08-02T16:00:00.000Z",
 *        "mode_of_payment": "bank_deposit",
 *        "bd_bank_account": "BPI",
 *        "is_collected": 0,
 *        "delivery_address": "Metro Manila, PH",
 *        "mode_of_delivery": "Courier via Rider",
 *        "agent_sales_person": "Jane Doe"
 *      }
 *    ],
 *    "pagination": {
 *      "TotalData": 1,
 *      "TotalPage": 1
 *    }
 *  },
 *  "success": true
 *}
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Initial payments not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

  /**
 * @api {get} /collections Retrieve Collections
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date by issuance_date, date_created, effectivity_date
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [is_cancel]             Policy status (yes, no)
 * 
 * @apiParamExample Sample-Params:
 * 
 * page=1&per_page=10&filter_date=date_created&filter_date_from=2021-08-27&filter_date_to=2021-08-30&is_cancel=no
 * 
 * @apiSuccess {Number}   sales_id          Sales ID
 * @apiSuccess {String}   policy_no         Policy number
 * @apiSuccess {String}   assured_name      Assured name
 * @apiSuccess {String}   car_make_model    Insured vehicle
 * @apiSuccess {String}   email_address     Client's name
 * @apiSuccess {String}   effectivity_date  Effective date
 * @apiSuccess {String}   last_payment_date Date of last payment
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *  "data": {
 *    "result": [
 *      {
 *       "sales_id": 1,
 *       "policy_no": "TST-PC-0826",
 *       "assured_name": "John Doe",
 *       "car_make_model": "2019 NISSAN TERRA 2.5 VL AT 4x4",
 *       "email_address": "user@email.com",
 *       "effectivity_date": "2021-09-03T16:00:00.000Z",
 *       "last_payment_date": null
 *     }
 *    ],
 *    "pagination": {
 *      "TotalData": 1,
 *      "TotalPage": 1
 *    }
 *  },
 *  "success": true
 *}
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Collections not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

 /**
 * @api {get} /collections/property/:sales_id Retrieve Collection Property
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {Number}   sales_id              Sales ID
 * @apiSuccess {String}   policy_no             Policy number
 * @apiSuccess {String}   next_follow_up_date   Next follow up date
 * @apiSuccess {String}   assured_name          Assured name
 * @apiSuccess {String}   receipt_number        Receipt number
 * @apiSuccess {String}   notes                 Receipt notes
 * @apiSuccess {String}   collections_officer   Collection officcer
 * @apiSuccess {String}   pullout_date          Pull-out date
 * @apiSuccess {String}   payment_memo          Payment memo
 * @apiSuccess {String}   collection_notes      Collection notes
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "sales_id": 1,
 *     "policy_no": "TST-PC-083021",
 *     "next_follow_up_date": null,
 *     "assured_name": "John Doe",
 *     "receipt_number": null,
 *     "notes": null,
 *     "collections_officer": null,
 *     "pullout_date": null,
 *     "payment_memo": "DONE PAYMENT",
 *     "collection_notes": null
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Collection property not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

 /**
 * @api {put} /collections/property/:policy_no Update Collection Property
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   policy_no               Policy number
 * @apiParam {String}   collection_notes        Collection notes
 * @apiParam {String}   next_follow_up_date     Next follow up date
 * @apiParam {String}   payment_memo            Payment memo
 * 
 * @apiParamExample Sample-Params:
 * 
 * {
 *     "collection_notes": "New Collection Note",
 *     "next_follow_up_date": "2021-09-01",
 *     "payment_memo": "New payment memo"
 * }
 * 
 * @apiSuccess {String}   policy_no             Policy number
 * @apiSuccess {String}   collection_notes      Collection notes
 * @apiSuccess {String}   next_follow_up_date   Next follow up date
 * @apiSuccess {String}   payment_memo          Payment memo
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully update a collection property",
 *     "result": {
 *       "policy_no": "TST-PCD-0817",
 *       "collection_notes": "New Collection Note",
 *       "next_follow_up_date": "2021-09-01",
 *       "payment_memo": "New payment memo"
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "NO_RECORD_UPDATED",
 *       "message": "No record was updated",
 *       "context": "No collection property was updated"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

 /**
 * @api {get} /collection/:sales_id Retrieve Collection By Id
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {Number}   id                    Collection ID
 * @apiSuccess {Number}   sales_id              Sales ID
 * @apiSuccess {String}   receipt_number        Receipt number
 * @apiSuccess {String}   pullout_date          Pullout Date
 * @apiSuccess {String}   collections_officer   Collections officer
 * @apiSuccess {String}   notes                 Collection notes
 * @apiSuccess {String}   date_created          Date Created
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "id": 1416,
 *     "sales_id": 40154,
 *     "receipt_number": "18050",
 *     "pullout_date": "2021-07-13T16:00:00.000Z",
 *     "collections_officer": "Jane Doe",
 *     "notes": "Endorsed to logistic team.",
 *     "date_created": "2021-07-13T18:51:41.000Z"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Collection record not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

 /**
 * @api {post} /collection Add Collection Record
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   sales_id                Sales ID
 * @apiParam {String}   receipt_number          Receipt number
 * @apiParam {String}   collections_officer     Collection officer
 * @apiParam {String}   pullout_date            Pull-out Date
 * @apiParam {String}   notes                   Notes
 * 
 * @apiParamExample Sample-Params:
 * 
 * {
 *     "sales_id": 1,
 *     "receipt_number": "TSTR-1234",
 *     "collections_officer": "Jane Doe",
 *     "pullout_date": "2021-08-02",
 *     "notes": "This is a sample note"
 * }
 * 
 * @apiSuccess {Number}   id                    Collection Id
 * @apiSuccess {Number}   sales_id              Sales Id
 * @apiSuccess {String}   receipt_number        Receipt number
 * @apiSuccess {String}   pullout_date          Pull-out date
 * @apiSuccess {String}   collections_officer   Collection officer
 * @apiSuccess {String}   notes                 Collection notes
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully created a collection record",
 *     "id": 1417,
 *     "sales_id": 40075,
 *     "receipt_number": "TSTR-1234",
 *     "pullout_date": "2021-08-02",
 *     "collections_officer": "Jane Doe",
 *     "notes": "This is a sample note"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "INC_DATA",
 *       "message": "Incomplete request data",
 *       "context": "receipt_number is missing"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */

/**
 * @api {put} /collection/:id Update Collection Record
 * @apiGroup Collection
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   [receipt_number]          Receipt number
 * @apiParam {String}   [collections_officer]     Collection officer
 * @apiParam {String}   [pullout_date]            Pull-out Date
 * @apiParam {String}   [notes]                   Notes
 * 
 * @apiParamExample Sample-Params:
 * 
 * {
 *     "notes": "This is updated sample note"
 * }
 * 
 * @apiSuccess {String}   [receipt_number]        Receipt number
 * @apiSuccess {Number}   [pullout_date]          Pull-out date
 * @apiSuccess {String}   [collections_officer]   Collection officer
 * @apiSuccess {String}   [notes]                 Notes
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully updated a collection record",
 *     "notes": "This is updated sample note"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "NO_RECORD_UPDATED",
 *       "message": "No record was updated",
 *       "context": "Collection record was not updated"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */
