/**
 * @api {get} /remittance/record Retrieve Remittance Record 
 * @apiGroup Remittance
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date (date_of_remittance, cheque_date)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [remittance_type]       Remittance type (gross_remittance, net_remittance)
 * @apiParam {String}   [insurance_quoted]      Insurance code
 * 
 * @apiParamExample Sample-Params:
 * 
 * page=1&per_page=10&search=test&filter_date=date_of_remittance
 * 
 * @apiSuccess {Number}   remittance_record_id  Remittance record Id
 * @apiSuccess {String}   remittance_ref_no     Remittance reference number
 * @apiSuccess {String}   remittance_type       Remittance type
 * @apiSuccess {String}   date_of_remittance    Date of remittance
 * @apiSuccess {String}   already_process       Is already process? (yes. no)
 * @apiSuccess {String}   insurance_name        Insurance company
 * @apiSuccess {String}   insurance_quoted      Code of Insurance company
 * @apiSuccess {String}   cheque_account_name   Cheque account name
 * @apiSuccess {String}   cheque_number         Cheque number
 * @apiSuccess {String}   cheque_date           Cheque date
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "remittance_record_id": 1,
 *         "remittance_ref_no": "2021 NET REMIT 4",
 *         "remittance_type": "net_remittance",
 *         "date_of_remittance": "2021-09-28T16:00:00.000Z",
 *         "already_process": "no",
 *         "insurance_name": "MAA Insurance co",
 *         "insurance_quoted": "MAA",
 *         "cheque_account_name": "BDO",
 *         "cheque_number": "123",
 *         "cheque_date": "2021-09-28T16:00:00.000Z"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Remittance Record not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */


/**
 * @api {get} /remittance/record/pool Retrieve Remittance Policy Pool 
 * @apiGroup Remittance
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date (date_created, issuance_date, effectivity_date)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [sales_remittance_type] Sales Remittance type (gross_remittance, net_remittance)
 * @apiParam {String}   [actual_fully_paid]     Actual fully paid? (yes, no)
 * @apiParam {String}   [is_cancel]             Cancelled policy (yes, no)
 * @apiParam {String}   [is_net_remitted]       Is net remitted (no, no_and_partial, partial, yes)
 * 
 * @apiParamExample Sample-Params:
 * 
 * actual_fully_paid=yes&filter_date=date_created&is_cancel=no&is_net_remitted=no_and_partial&insurance_quoted=MC&sales_remittance_type=net_remittance
 * 
 * @apiSuccess {String}   policy_no             Sales policy number
 * @apiSuccess {String}   assured_name          Client's name
 * @apiSuccess {String}   car_make_model        Insured vehicle
 * @apiSuccess {String}   email_address         Client's email address
 * @apiSuccess {String}   effectivity_date      Effective date
 * @apiSuccess {String}   last_payment_date     Last payment date
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "policy_no": "1020101210001587",
 *         "assured_name": "John Doe",
 *         "car_make_model": "2021 TOYOTA VIOS",
 *         "email_address": "user@email.com",
 *         "effectivity_date": "2021-09-28T16:00:00.000Z",
 *         "last_payment_date": "2021-09-228T16:00:00.000Z"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Sales policy not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */


/**
 * @api {get} /remittance/policy Retrieve Remittance on Policy per Insurance
 * @apiGroup Remittance
 * @apiVersion 1.0.0
 * 
 * @apiParam {Number}   [page=1]                Page number
 * @apiParam {Number}   [per_page=10]           Data per page
 * @apiParam {String}   [search]                Search keyword
 * @apiParam {String}   [filter_date]           Filter date (date_created, issuance_date, effectivity_date)
 * @apiParam {String}   [filter_date_from]      Filter date start range
 * @apiParam {String}   [filter_date_to]        Filter date end range
 * @apiParam {String}   [sales_remittance_type] Sales Remittance type (gross_remittance, net_remittance)
 * @apiParam {String}   [actual_fully_paid]     Actual fully paid? (yes, no)
 * @apiParam {String}   [is_cancel]             Cancelled policy (yes, no)
 * @apiParam {String}   [is_net_remitted]       Is net remitted (no, no_and_partial, partial, yes)
 * 
 * @apiParamExample Sample-Params:
 * 
 * actual_fully_paid=yes&filter_date=date_created&is_cancel=no&is_net_remitted=no_and_partial&insurance_quoted=MC&sales_remittance_type=net_remittance
 * 
 * @apiSuccess {String}   policy_no             Sales policy number
 * @apiSuccess {String}   insurance_name        Insurance company
 * @apiSuccess {String}   assured_name          Clien'ts name
 * @apiSuccess {String}   remittance_type       Remittance type
 * @apiSuccess {Number}   actual_balance        Actual balance
 * @apiSuccess {String}   is_net_remitted       Is net remitted? (yes, no)
 * @apiSuccess {String}   is_gross_remitted     Is gross remitted? (yes, no)
 * @apiSuccess {String}   date_of_remittance    Date of remittance
 * @apiSuccess {Number}   gross_premium         Gross premium
 * @apiSuccess {Number}   total_premium         Total premium
 * @apiSuccess {String}   issuance_date         Issuance date
 * @apiSuccess {String}   effectivity_date      Effective date
 * @apiSuccess {String}   date_created          Date created
 * @apiSuccess {Object[]} pagination                Pagination info
 * @apiSuccess {Number}   pagination.TotalData      Total Data
 * @apiSuccess {Number}   pagination.TotalPage      Total Page
 * 
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "result": [
 *       {
 *         "policy_no": "SI-PMM-00000123123",
 *         "insurance_name": "Standard Insurance",
 *         "assured_name": "John Doe",
 *         "remittance_type": null,
 *         "actual_balance": -5.369529999999941,
 *         "is_net_remitted": "no",
 *         "is_gross_remitted": "yes",
 *         "date_of_remittance": null,
 *         "gross_premium": 24294.01,
 *         "total_premium": 30294.63047,
 *         "issuance_date": "2021-09-28T16:00:00.000Z",
 *         "effectivity_date": "2021-09-28T16:00:00.000Z",
 *         "date_created": "2021-09-28T03:57:52.000Z"
 *       }
 *     ],
 *     "pagination": {
 *       "TotalData": 1,
 *       "TotalPage": 1
 *     }
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *     {
 *       "code": "ZERO_RES",
 *       "message": "Database returned no result",
 *       "context": "Sales policy not found"
 *     }
 *   ],
 *   "success": false
 * }
 * 
 *
 */


/**
 * @api {post} /remittance/record Create Remittance Record
 * @apiGroup Remittance
 * @apiVersion 1.0.0
 * 
 * @apiParam {String}   date_of_remittance      Date of remittance
 * @apiParam {String}   insurance_name          Insurance company
 * @apiParam {String}   insurance_quoted        Insurance company code
 * @apiParam {String}   policy_role             Ppolicy role
 * @apiParam {String}   remittance_ref_no       Remittance reference number
 * @apiParam {String}   remittance_type         Remittance type (net_remittance, gross_remittance)
 * @apiParam {String}   [cheque_account_name]   Cheque account name (for net_remittance only)
 * @apiParam {String}   [cheque_date]           Cheque date (for net_remittance only)
 * @apiParam {String}   [cheque_number]         Cheque number (for net_remittance only)
 * 
 * @apiParamExample Sample-Params:
 * {
 *     "cheque_account_name": "BDO",
 *     "cheque_date": "2021-09-28",
 *     "cheque_number": "TSTREMIT-0928",
 *     "date_of_remittance": "2021-09-28",
 *     "insurance_name": "Standard Insurance",
 *     "insurance_quoted": "SI",
 *     "policy_role": "ichoose",
 *     "remittance_ref_no": "TST NET REMIT 28",
 *     "remittance_type": "net_remittance"
 * }
 *
 * 
 * @apiSuccess {String}   id                Remittance record ID
 * @apiSuccess {String}   insurance_name    Insurance company
 * @apiSuccess {String}   insurance_quoted  Insurance company code
 * @apiSuccess {String}   remittance_ref_no Remittance reference number
 *
 * @apiSuccessExample Sample-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *     "message": "Successfully created remittance record",
 *     "id": 4954,
 *     "insurance_name": "Standard Insurance",
 *     "insurance_quoted": "SI",
 *     "remittance_ref_no": "TST NET REMIT 28"
 *   },
 *   "success": true
 * }
 *
 * @apiErrorExample Sample-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *   "errors": [
 *       {
 *           "code": "INVALID_REMITTANCE_REF_NO",
 *           "message": "Reference number is already in use",
 *           "context": "Reference number is already in use"
 *       }
 *   ],
 *   "success": false
 * }
 * 
 *
 */