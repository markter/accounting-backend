'use strict';

const crypto  = require('crypto');
const config  = require(__dirname + '/../config/config');

const ENCRYPTION_KEY = Buffer.from(config.CRYPTO_BUFFER, 'base64');

exports.encrypt = (data, callback) => {
    
    const cipher    = crypto.createCipher(config.ENCRYPT, config.SALT);
    let encrypted   = cipher.update(data, 'utf8', 'hex');
    encrypted      += cipher.final('hex');
    
    callback(encrypted);
};

exports.decrypt = (data, callback) => {

    const decipher = crypto.createDecipher(config.ENCRYPT, config.SALT);
    let decrypted  = decipher.update(data, 'hex', 'utf8');
    decrypted     += decipher.final('utf8');
    decrypted      = JSON.parse(decrypted);

    callback(decrypted);
};

exports.encryptSync = (data) => {

    if (typeof data === 'object' ) {
        data  = JSON.stringify(data);
    } 

    else if (typeof data === 'number') {
        data  = data.toString();
    } 

    else if (typeof data !== 'string') {
        throw new TypeError('Data must be object or number');
    } 

    const cipher    = crypto.createCipher(config.ENCRYPT, config.SALT);
    let encrypted   = cipher.update(data, 'utf8', 'hex');
    encrypted      += cipher.final('hex');
    
    return encrypted;
};

exports.decryptSync = (data) => {

    const decipher = crypto.createDecipher(config.ENCRYPT, config.SALT);
    let decrypted  = decipher.update(data, 'hex', 'utf8');
    decrypted     += decipher.final('utf8');

    try {
        decrypted  = JSON.parse(decrypted);
    } catch (e) {

        try {
            decrypted  = parseInt(decrypted);
        } catch (e) {
            return decrypted;
        }

    }

    return decrypted;
};

exports.encryptSyncNew = (data) => {

    if (typeof data === 'object' ) data  = JSON.stringify(data);

    if (typeof data === 'number') data  = data.toString();

    if (typeof data !== 'string') throw new TypeError('Data must be object or number');
    
    let iv          = crypto.randomBytes(config.IV_LENGTH),
        cipher      = crypto.createCipheriv(config.CRYPTO_ALGO, Buffer.from(ENCRYPTION_KEY, 'hex'), iv),
        encrypted   = cipher.update(data);
    
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    
    return iv.toString('hex') + ':' + encrypted.toString('hex');

};

exports.decryptSyncNew = (data) => {

    let textParts       = data.split(':'),
        iv              = Buffer.from(textParts.shift(), 'hex'),
        encryptedText   = Buffer.from(textParts.join(':'), 'hex'),
        decipher        = crypto.createDecipheriv(config.CRYPTO_ALGO, Buffer.from(ENCRYPTION_KEY, 'hex'), iv),
        decrypted       = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    try {
        decrypted  = JSON.parse(decrypted);
    } catch (e) {

        try {
            decrypted  = parseInt(decrypted);
        } catch (e) {
            return decrypted;
        }

    }
    
    return decrypted;

};