'use strict';

const mysql   = require('anytv-node-mysql');
const Q       = require('q');
const winston = require('winston');

function log (req) {
    let deferred = Q.defer();

    if (req.body.user === undefined) {
        req.body.user = {id: 'GUEST', email: 'GUEST'};
    }

    //! OLD ACTIVITY LOGGER => DB
    /* mysql.use('log')
        .query(
            `INSERT INTO log (userId, actor, method, activity, ipAddress)
             VALUES (?, ?, ?, ?, ?)`,
            [req.body.user.id, 
             req.body.user.email, 
             req.originalMethod,
             req.url,
             req.ip
            ],
            (err, result) => {
                if (err) {
                    winston.info(`Error in inserting log`, err);
                    return deferred.reject(err);
                }

                deferred.resolve(result.insertId);
            }
        )
        .end();

    return deferred.promise; */

    mysql.use('log')
    .query(
        `
            INSERT INTO log (userId, actor, method, activity, ipAddress)
            VALUES (?, ?, ?, ?, ?)
        `,
        [
            req.body.user.id,
            req.body.user.email,
            req.originalMethod,
            req.url,
            req.ip
        ],
        (err, result) => {
            if(err) {
                winston.info(`Error in inserting log`, err);
                return err;
            }

            return result;
        }
    )
    .end();

}

function log_end (queryId, remarks) {
    let deferred = Q.defer();

    //! OLD LOGGER CODE
    /* mysql.use('log')
        .query(
            'UPDATE log SET remarks = ? WHERE id = ?',
            [remarks, queryId],
            (err, result) => {
                if (err) {
                    winston.info(`Error in updating log`, err);
                    return deferred.reject(err);
                }

                deferred.resolve(result.insertId);
            }
        )
        .end();

    return deferred.promise; */
    
    mysql.use('log')
    .query(
        `
            UPDATE  log
            SET     remarks = '${ remarks }'
            WHERE   id = '${ queryId }'
        `,
        (err, result) => {
            if (err) {
                winston.info(`Error in updating log`, err);
                return err;
            }

            return result;
        }
    )
    .end();

}

module.exports = {
    log,
    log_end
};