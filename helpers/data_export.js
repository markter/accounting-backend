const Workbook  = require('xlsx-populate');

exports.sales_export = (req, res, next) => {

    const workbook  = Workbook.fromFileSync(__dirname + "/../exceltemplate/template_sales_report.xlsx");
    const outfile   = __dirname + "/../exceltemplate/salespolicy_report.xlsx";

    const column_order = [
        'car_make_model', 'reference_quote_no',
        'mortgagee', 'cv_pc', 'policy_no',
        'insurance_name', 'address', 'assured_name',
        'category', 'issued_on', 'template',
        'promotions', 'agent_sales_person',
        'contact_no', 'gross_premium',
        'total_premium', 'issuance_date',
        'effectivity_date', 'premium_variance',
        'fmv', 'od_premium', 'aon_premium',
        'bi_pd_premium', 'pa_premium',
        'docstamps', 'lgt', 'evat',
        'od_theft_comm', 'aon_comm',
        'bi_pd_comm', 'total_comm_compute',
        'total_comm_manual', 'ichoose_comm',
        'bestlink_comm', 'salespolicy_notes',
        'actual_balance'
    ];

    let json_data = req.body.sales_data;

    let sheet = workbook.getSheet(0),
        start_row_at = 7,
        end_row_num,
        sub_total_row_num,
        column_name = '';

    sheet.getCell("A2").setValue('ICHOOSE AGENCY');

    for (var rowNum = 0; rowNum < json_data.length; rowNum++) {
        var row = sheet.getRow(rowNum+start_row_at);
        for (var colNum = 1; colNum <= column_order.length; colNum++) {

            column_name = column_order[(colNum -1)];
            var cell = row.getCell(colNum);
            cell.setValue(json_data[rowNum][column_name]);
        }
    }

    end_row_num = start_row_at + rowNum;
    sub_total_row_num = end_row_num + 5;

    sheet.getCell("I" + sub_total_row_num).setValue("SUBTOTAL");
    sheet.getCell("J" + sub_total_row_num).setFormula("SUM(J"+start_row_at+":" + "J" +end_row_num+")");
    sheet.getCell("K" + sub_total_row_num).setFormula("SUM(K"+start_row_at+":" + "K" +end_row_num+")");
    
    sheet.getCell("Z" + sub_total_row_num).setFormula("SUM(Z"+start_row_at+":" + "Z" +end_row_num+")");
    sheet.getCell("AA" + sub_total_row_num).setFormula("SUM(AA"+start_row_at+":" + "AA" +end_row_num+")");
    sheet.getCell("AB" + sub_total_row_num).setFormula("SUM(AA"+start_row_at+":" + "AB" +end_row_num+")");
    sheet.getCell("AC" + sub_total_row_num).setFormula("SUM(AC"+start_row_at+":" + "AC" +end_row_num+")");
    
    workbook.toFileSync(outfile); 

    res.download(outfile, 'report.xlsx', (err) => {
        if (err) {
            winston.error('Error in sales export');
            return next(err);
        }
    });

}


exports.issuance_export = (req, res, next) => {

    const workbook  = Workbook.fromFileSync(__dirname + "/../exceltemplate/template_issuance.xlsx");
    const outfile   = __dirname + "/../exceltemplate/issuance_report.xlsx";

    const column_order = [
        'date_created',
        'reference_number',
        'name',
        'address',
        'delivery_address',
        'post_code',
        'email',
        'mobile',
        'phone1',
        'phone2',                
        'car_insurance_for',
        'vin_chassi_no',
        'motor_engine_no',
        'plate_conduction_no',
        'color',
        'effectivity_date',
        'mortgagee',
        'insurance_company',
        'fair_market_value',
        'rate',
        'acts_of_god_rate',
        'bodily_injury_property_damage',
        'acts_of_god_format',
        'promotions',
        'classification',
        'memo',
        'od_premium',
        'aon_premium',
        'od_aon_premium',
        'bi_pd_premium',
        'pa_premium',
        'lto_interconnectivity',
        'docstamps',
        'lgt',
        'evat',
        'annual_premiums',
        'total_premiums',
        'agent_sales_person',
        'category',
        'location'
    ];

    let json_data = req.body.issuance_data;

    let sheet = workbook.getSheet(0),
        start_row_at = 7,
        end_row_num,
        sub_total_row_num,
        column_name = '';

    sheet.getCell("A2").setValue('ICHOOSE AGENCY');

    for (var rowNum = 0; rowNum < json_data.length; rowNum++) {
        var row = sheet.getRow(rowNum+start_row_at);
        for (var colNum = 1; colNum <= column_order.length; colNum++) {

            column_name = column_order[(colNum -1)];
            var cell = row.getCell(colNum);
            cell.setValue(json_data[rowNum][column_name]);
        }
    }

    end_row_num = start_row_at + rowNum;
    sub_total_row_num = end_row_num + 5;

    workbook.toFileSync(outfile); 

    res.download(outfile, 'report.xlsx', (err) => {
        if (err) {
            winston.error('Error in sales export');
            return next(err);
        }
    });

}

exports.initial_payments_export = (req, res, next) => {

    const workbook  = Workbook.fromFileSync(__dirname + "/../exceltemplate/template_initial_payments.xlsx");
    const outfile   = __dirname + "/../exceltemplate/initial_payments_report.xlsx";

    const column_order = [
        'date_received', 'mode_of_payment', 'amount_received', 
        'assured_name', 'bd_bank_account', 'bd_date_deposited',
        'policy_no', 'reference_quote_no', 'delivery_address',
        'receipt_number', 'pullout_date', 'collections_officer',
        'notes'
    ];

    let json_data = req.body.issuance_data;

    let sheet = workbook.getSheet(0),
        start_row_at = 7,
        end_row_num,
        sub_total_row_num,
        column_name = '';

    sheet.getCell("A2").setValue('ICHOOSE AGENCY');

    for (var rowNum = 0; rowNum < json_data.length; rowNum++) {
        var row = sheet.getRow(rowNum+start_row_at);
        for (var colNum = 1; colNum <= column_order.length; colNum++) {

            column_name = column_order[(colNum -1)];
            var cell = row.getCell(colNum);
            cell.setValue(json_data[rowNum][column_name]);
        }
    }

    end_row_num = start_row_at + rowNum;
    sub_total_row_num = end_row_num + 5;

    workbook.toFileSync(outfile); 

    res.download(outfile, 'initial_payments.xlsx', (err) => {
        if (err) {
            winston.error('Error in sales export');
            return next(err);
        }
    });

}

exports.remittance_export = (req, res, next) => {

    const workbook  = Workbook.fromFileSync(__dirname + "/../exceltemplate/template_remiitanceperinsurance_report.xlsx");
    const outfile   = __dirname + "/../exceltemplate/remittancepersinsurance_report.xlsx";

    const column_order = [
        'policy_no', 'insurance_name',
        'assured_name', 'gross_premium',
        'total_premium', 'issuance_date',
        'effectivity_date', 'date_created',
        'actual_balance', 'is_net_remitted',
        'sum_payment_net_remittance',
        'netremittancedata-0-remittance_ref_no',
        'netremittancedata-0-date_of_remittance',
        'is_gross_remitted', 'sum_payment_gross_remittance',
        'grossremittancedata-0-remittance_ref_no',
        'grossremittancedata-0-date_of_remittance'
    ];

    let sales_data = req.body.sales_data;
        remittance_data = req.body.remittance_data;
        gross_remittance_data = req.body.gross_remittance_data,
        insurance_quoted = req.body.insurance_quoted;

    let sheet = workbook.getSheet(0),
        start_row_at = 7,
        end_row_num,
        sub_total_row_num,
        column_name = '';

    sheet.getCell("A2").setValue('ICHOOSE AGENCY');

    for (var i = 0; i < sales_data.length; i++) {

        sales_id_res = sales_data[i]['sales_id'];
        
        var netArray = remittance_data.filter(function(o) {
          return (o.sales_id == sales_id_res);
        });

        sales_data[i].netremittancedata = netArray;

        var grossArray = gross_remittance_data.filter(function(o) {
          return (o.sales_id == sales_id_res);
        });

        sales_data[i].grossremittancedata = grossArray;

    }

    if (insurance_quoted) sheet.getCell("A3").setValue(insurance_quoted);
    
    for (var rowNum = 0; rowNum < sales_data.length; rowNum++) {

        var row = sheet.getRow(rowNum + start_row_at);

        for (var colNum = 1; colNum <= column_order.length; colNum++) {

            column_name = column_order[(colNum -1)];

            var column_net_name = column_name.split('-')[0];
            var column_net_index = column_name.split('-')[1];
            var column_net_field = column_name.split('-')[2];

            var column_gross_name = column_name.split('-')[0];
            var column_gross_index = column_name.split('-')[1];
            var column_gross_field = column_name.split('-')[2];

            var cell = row.getCell(colNum);

            if (column_net_name == 'netremittancedata') {
                if (parseInt(column_net_index) <  sales_data[rowNum]['netremittancedata'].length)
                    cell.setValue(sales_data[rowNum]['netremittancedata'][column_net_index][column_net_field]);
            } else if (column_net_name == 'grossremittancedata') {
                if (parseInt(column_gross_index) <  sales_data[rowNum]['grossremittancedata'].length)
                    cell.setValue(sales_data[rowNum]['grossremittancedata'][column_gross_index][column_gross_field]);
            } 
            else {
                cell.setValue(sales_data[rowNum][column_name]);
            }
        }
    } 

    end_row_num = start_row_at + rowNum;
    sub_total_row_num = end_row_num + 5;

    sheet.getCell("C" + sub_total_row_num).setValue("SUBTOTAL");
    sheet.getCell("D" + sub_total_row_num).setFormula("SUM(D"+start_row_at+":" + "D" +end_row_num+")");
    sheet.getCell("E" + sub_total_row_num).setFormula("SUM(E"+start_row_at+":" + "E" +end_row_num+")");
    sheet.getCell("I" + sub_total_row_num).setFormula("SUM(I"+start_row_at+":" + "I" +end_row_num+")");
    sheet.getCell("K" + sub_total_row_num).setFormula("SUM(K"+start_row_at+":" + "K" +end_row_num+")");
    sheet.getCell("O" + sub_total_row_num).setFormula("SUM(O"+start_row_at+":" + "O" +end_row_num+")");

    workbook.toFileSync(outfile); 

    res.download(outfile, 'report.xlsx', (err) => {
        if (err) {
            winston.error('Error in sales export');
            return next(err);
        }
    });

}