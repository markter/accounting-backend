const fs            = require('fs');
const path          = require('path');
const nodemailer    = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const templates     = require('email-templates');

const config        = require(__dirname + '/../config/config');
const directory     = path.join(__dirname, '/../email-template');

const gmail = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth:{
        user: config.USER,
        pass: config.PASS
    }
}));

function email_notification (to, details, callback) {

    templates(directory, function(err, template) {

        if (err) {
            return callback(err, null);
        }

        template('email-gcash-ctpl', details, function(err, html, text) {

            if(err) {
                return callback(err, null);
            }
            
            gmail.sendMail({
                from: '',
                cc: '',
                bcc: '',
                to: to,
                subject: '',
                html: html,
                attachments: [{
                    filename: '',
                    content: fs.createReadStream('')
                }]
            }, callback);

        });

    });

}

module.exports = {
    email_notification
}